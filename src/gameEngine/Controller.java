package gameEngine;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Classe qui represente un controleur en lien avec un KeyListener
 */
public class Controller implements KeyListener {

	private Command currentCommand;
	private Command commandToReturn;
	private String cheatCode;

	/**
	 * construction du controleur par defaut le controleur n'a pas de commande
	 */
	public Controller() {
		this.cheatCode = "";
		this.currentCommand = new Command();
		this.commandToReturn = new Command();
	}

	public Command getCommand() {
		Command toReturn = this.commandToReturn;
		this.commandToReturn = new Command(this.currentCommand);
		return (toReturn);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyChar()) {
		case 'q':
			this.currentCommand.left = true;
			this.commandToReturn.left = true;
			break;
		case 'd':
			this.currentCommand.right = true;
			this.commandToReturn.right = true;
			break;
		case 'z':
			this.currentCommand.up = true;
			this.commandToReturn.up = true;
			break;
		case 's':
			this.currentCommand.down = true;
			this.commandToReturn.down = true;
			break;
		case '&':
			this.currentCommand.item = -1;
			this.commandToReturn.item = 0;
			break;
		case 'é':
			this.currentCommand.item = -1;
			this.commandToReturn.item = 1;
			break;
		case '"':
			this.currentCommand.item = -1;
			this.commandToReturn.item = 2;
			break;
		case '\'':
			this.currentCommand.item = -1;
			this.commandToReturn.item = 3;
			break;
		case '(':
			this.currentCommand.item = -1;
			this.commandToReturn.item = 4;
			break;
		case '-':
			this.currentCommand.item = -1;
			this.commandToReturn.item = 5;
			break;
		case 'è':
			this.currentCommand.item = -1;
			this.commandToReturn.item = 6;
			break;
		case '_':
			this.currentCommand.item = -1;
			this.commandToReturn.item = 7;
			break;
		case 'ç':
			this.currentCommand.item = -1;
			this.commandToReturn.item = 8;
			break;
		case 'à':
			this.currentCommand.item = -1;
			this.commandToReturn.item = 9;
			break;
		}

		switch(e.getKeyCode()){
		case KeyEvent.VK_SPACE:
			this.currentCommand.attack = true;
			this.commandToReturn.attack = true;
			break;
		case KeyEvent.VK_ESCAPE:
			this.currentCommand.pause = !this.currentCommand.pause;
			this.commandToReturn.pause = this.currentCommand.pause;
		}
		
		this.cheatCode+=e.getKeyChar();
		if(this.cheatCode.contains("labitbucket")){
			this.activateLabitbucketCode();
		}
		else if(this.cheatCode.contains("mynameistomtombinary")){
			this.activateTomtomCode();
		}
		
		if(this.cheatCode.length()>100)
			this.cheatCode = "";
	}
	
	private void activateLabitbucketCode()
	{
		this.currentCommand.labitbucket = false;
		this.commandToReturn.labitbucket = true;
		this.cheatCode = "";
	}
	
	private void activateTomtomCode()
	{
		this.currentCommand.mynameistomtombinary = false;
		this.commandToReturn.mynameistomtombinary = true;
		this.cheatCode = "";
	}

	@Override
	public void keyReleased(KeyEvent e) {
		switch (e.getKeyChar()) {
		case 'q':
			this.currentCommand.left = false;
			break;
		case 'd':
			this.currentCommand.right = false;
			break;
		case 'z':
			this.currentCommand.up = false;
			break;
		case 's':
			this.currentCommand.down = false;
			break;
		}

		switch(e.getKeyCode()){
		case KeyEvent.VK_SPACE:
			this.currentCommand.attack = false;
			break;
		case KeyEvent.VK_ESCAPE:
			this.currentCommand.pause = false;
			break;
		}
	}

	public void reset(){
		this.currentCommand = new Command();
		this.commandToReturn = new Command();
	}
	@Override
	public void keyTyped(KeyEvent e) {}
}
