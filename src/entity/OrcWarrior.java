package entity;


import behaviours.FightWithSpear;
import environment.Labyrinth;
import graphicEngine.CharacterSprite;
import myGame.RessourceLoader;

public class OrcWarrior extends Monster{

	public OrcWarrior(Labyrinth laby){
		super(RessourceLoader.getOrcSprite(),laby);
		this.setLife(40);
		this.fight = new FightWithSpear((CharacterSprite) this.sprite);
		this.damages = 10;
	}

	@Override
	public String monsterType() {
		return Monster.ORC_WARRIOR;
	}

	@Override
	public String xmlType() {
		return "OrcWarrior";
	}
}