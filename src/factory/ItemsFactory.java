package factory;

import items.Item;

import java.util.ArrayList;

import environment.Labyrinth;

public interface ItemsFactory {
	public abstract ArrayList<Item> generateItems(Labyrinth labyrinth);
}
