package entity;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import graphicEngine.CharacterSprite;
import ia.Action;
import ia.IA;
import environment.Labyrinth;

public abstract class Monster extends Character{

	public static final String ORC_WARRIOR="Guerrier Orc",ENEMY_MAGE = "Mage",ARCHER_SKELETON = "Archer Squelette";
	protected IA ia;
	
	public Monster(CharacterSprite sprite, Labyrinth labyrinth) {
		super(sprite, labyrinth);
	}
	
	@Override
	public void update(long delta_time){
		if(ia!=null){
			Action action = ia.decide();
			this.orientate(action.orientation);
			this.setAction(action.command);
		}
		super.update(delta_time);
	}
	
	public void setIA(IA ia){
		this.ia = ia;
		this.ia.setSensor(this);
	}
	
	public abstract String xmlType();
	public abstract String monsterType();
	
	@Override
	public Element toXML(Document doc){
		Element monster = super.toXML(doc);
		monster.setAttribute("type",xmlType());
		Element ia_type = ia.toXML(doc);
		monster.appendChild(ia_type);
		return monster;
	}
	
	@Override
	public String XMLEntityName(){
		return "monster";
	}
	
	@Override
	public String toString(){
		return "Monstre en "+super.toString()+" de type "+monsterType();
	}
}
