package graphicEngine;

import gameEngine.Updateable;

import java.awt.image.BufferedImage;

import myGame.RessourceLoader;

/**
 * Classe qui gere les Sprites de tous les Personnages
 */
public class CharacterSprite extends Sprite implements Updateable{

	public static final int UP=0, LEFT=1, DOWN=2, RIGHT=3;
	public static final int SPELL=0, SPEAR=1, MOVING=2, ATTACK=3, BOW=4, DIE=5;
	
	private int currentPlay;
	private int orientation;
	private AnimatedSprite states[][];
	
	/**
	 * Construit un Sprite animee pour un personnage
	 * @param source
	 *  source
	 * @param nFrame
	 *  nombre de frame par ligne
	 * @param width
	 *  taille du decoupage largeur
	 * @param height
	 *  taille du decoupage hauteur 
	 */
	public CharacterSprite(BufferedImage source, int width, int height){
		super(source);
		int newWidth = 13*width;
		int newHeight= 21*height;
		this.source = RessourceLoader.reSize(source, newWidth, newHeight);
		this.states = new AnimatedSprite[6][4];
		
		for(int i=0;i<4;i++){
			this.states[SPELL][i] = new AnimatedSprite(
					this.source.getSubimage(0,SPELL*height*4 + i*height,7*width,height),
					7,
					0,0,
					1,7
			);
			this.states[SPELL][i].setTimeWait(75);
		}
		
		for(int i=0;i<4;i++){
			this.states[SPEAR][i] = new AnimatedSprite(
					this.source.getSubimage(0,SPEAR*height*4+i*height,8*width,height),
					8,
					0,0,
					1,8
			);
			this.states[SPEAR][i].setTimeWait(65);
		}
		
		for(int i=0;i<4;i++){
			this.states[MOVING][i] = new AnimatedSprite(
					this.source.getSubimage(0,MOVING*height*4+i*height,9*width,height),
					9,
					0,0,
					1,9
			);
			this.states[MOVING][i].setTimeWait(75);
		}
		
		for(int i=0;i<4;i++){
			this.states[ATTACK][i] = new AnimatedSprite(
					this.source.getSubimage(0,ATTACK*height*4+i*height,6*width,height),
					6,
					0,0,
					1,6
			);
			this.states[ATTACK][i].setTimeWait(75);
		}
		
		for(int i=0;i<4;i++){
			this.states[BOW][i] = new AnimatedSprite(
					this.source.getSubimage(0,BOW*height*4+i*height,13*width,height),
					13,
					0,0,
					1,13
			);
			this.states[BOW][i].setTimeWait(65);
		}
		
		for(int i=0;i<4;i++){
			this.states[DIE][i] = new AnimatedSprite(
					this.source.getSubimage(0,DIE*height*4,6*width,height),
					6,
					0,0,
					1,6
				);
			this.states[DIE][i].setTimeWait(75);
		}
		this.current = this.states[MOVING][UP].getCurrent();
	}

	@Override
	public void update(long delta_time) {
		super.update(delta_time);
		states[currentPlay][orientation].update(delta_time);
		this.current = states[currentPlay][orientation].getCurrent();
	}
	
	public void play(int action,int orientation){
		if(action>=0 && action<6){
			if(currentPlay != action){
				this.currentPlay = action;
			}
		}
		
		if(orientation>=0 && orientation<4){
			if(this.orientation != orientation)
				this.orientation = orientation;
		}
		
		if(!states[currentPlay][orientation].isStarted())
			states[currentPlay][orientation].start();
	}
}
