package gui.launchGame;

import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import myGame.RessourceLoader;

public class PlayButton extends JButton {
	
	public PlayButton(ActionListener listener)
	{
		super(new ImageIcon(RessourceLoader.playButton));
		this.setPressedIcon(new ImageIcon(RessourceLoader.playButtonPressed));
		this.setRolloverIcon(new ImageIcon(RessourceLoader.playButtonRollover));
		this.setContentAreaFilled(false);
		this.setBorderPainted(false);
		this.setFocusPainted(false);
		this.addActionListener(listener);
	}
}