package graphicEngine;

import gameEngine.Updateable;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

public class ItemSprite extends Sprite implements Updateable{

	public static final int ITEM_SIZE=34;
	
	private float time_count;
	
	private float increment;
	private float variation;

	private int y_modifier;
	
	public ItemSprite(BufferedImage source){
		super(source);
		this.variation = 5;
		this.increment = 12;
	}
	
	
	public ItemSprite(BufferedImage source, float variation, float increment) {
		super(source);
		this.variation = variation;
		this.increment = increment;
	}
	
	@Override
	public void update(long delta_time){
		super.update(delta_time);
		this.time_count = (time_count+increment)%360;
		this.y_modifier = (int)(Math.cos(Math.toRadians(time_count))*variation);
	}
	
	@Override
	public void draw(Graphics2D g) {
		AffineTransform svg = g.getTransform(); //Sauvegarde la matrice de transformation
		g.scale(scaleX,scaleY);
		g.translate(posX,posY+this.y_modifier);
		g.rotate(theta);
		g.drawImage(current,-current.getWidth()/2,-current.getHeight()/2,null);
		g.setTransform(svg); //Restaure la matrice de transformation
	}
}
