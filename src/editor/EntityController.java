package editor;

import items.Item;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import entity.Entity;
import entity.Monster;
import environment.Labyrinth;
import environment.Tile;
import factory.LabyrinthEditor;

public class EntityController implements MouseMotionListener,MouseListener {

	private Entity entity;
	private LabyrinthEditor editor;

	public EntityController(Entity entity,Labyrinth labyrinth){
		this.entity = entity;
		this.editor = new LabyrinthEditor(labyrinth);
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		if(entity instanceof Tile){
			Tile tile_to_add = editor.createCaseFromId(entity.x,entity.y,((Tile)entity).getID());
			editor.addCaseToLabyrinthe(tile_to_add);
		}
		else if(entity instanceof Monster){
			Monster monsterToAdd = editor.createMonsterFromType(((Monster)entity).monsterType());
			monsterToAdd.setPosition(entity.x,entity.y);
			MonsterDialog dialog = new MonsterDialog(monsterToAdd);
			if(dialog.showDialog()){
				editor.addMonsterToLabyrinth(monsterToAdd);
			}
		}
		else if(entity instanceof Item){
			Item itemToAdd = editor.createItemFromName(((Item)entity).getName());
			itemToAdd.setPosition(entity.x,entity.y);
			editor.addItemToLabyrinth(itemToAdd);
		}
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		LabyrinthPanel panel = (LabyrinthPanel)arg0.getSource();
		int line  = arg0.getY()/panel.getCurrentTileSize();
		int column= arg0.getX()/panel.getCurrentTileSize();
		entity.setPosition(column,line);
		if(entity instanceof Tile){
			Tile tile_to_add = editor.createCaseFromId(entity.x,entity.y,((Tile)entity).getID());
			editor.addCaseToLabyrinthe(tile_to_add);
		}
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		LabyrinthPanel panel = (LabyrinthPanel)arg0.getSource();
		int line  = arg0.getY()/panel.getCurrentTileSize();
		int column= arg0.getX()/panel.getCurrentTileSize();
		entity.setPosition(column,line);
	}

	public void setEntity(Entity entity){
		this.entity = entity;
		this.entity.startEditing();
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {}
	@Override
	public void mouseReleased(MouseEvent arg0) {}
	@Override
	public void mouseEntered(MouseEvent arg0) {}
	@Override
	public void mouseExited(MouseEvent arg0) {}
}
