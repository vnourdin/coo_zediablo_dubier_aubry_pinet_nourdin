package timer;

import gameEngine.Updateable;

public abstract class TimerEffect implements Updateable{

	private boolean isStarted;
	private long sleep;
	private long timer;
	
	public TimerEffect(long sleep){
		this.isStarted = false;
		this.sleep = sleep;
		this.timer = 0;
	}
	
	public void start(){
		this.isStarted = true;
	}
	
	public boolean isStoped(){
		return !this.isStarted;
	}

	@Override
	public void update(long delta_time) {
		if(this.isStarted){
			this.timer += delta_time;
			if(this.timer > this.sleep){
				this.tick();
				this.isStarted = false;
				this.timer = 0;
			}
		}
	}
	
	public abstract void tick();
}
