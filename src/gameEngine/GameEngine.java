package gameEngine;

import graphicEngine.Drawable;
import graphicEngine.GraficInterface;

import javax.swing.JPanel;

public class GameEngine {

	private int updateTime = 25;

	private Game game;
	private GraficInterface gui;
	private Controller control;
	private Drawable drawing;
	private GameThread gameThread;

	public GameEngine(Game game, Drawable drawing, int width, int height) {
		// creation du jeu
		this.game = game;
		this.drawing = drawing;
		// creation de l'interface graphique
		this.gui = new GraficInterface(this.drawing, width, height);
		this.control = this.gui.getControleur();
	}

	public void startTheGame() throws InterruptedException {
		this.gui.activerControleur();
		this.gameThread = new GameThread();
		this.gameThread.start();
	}

	private class GameThread extends Thread {
		private boolean pause = false;

		public synchronized void unpause() {
			this.pause = false;
		}

		public boolean isPaused() {
			return this.pause;
		}

		@Override
		public void run() {
			long timeBefore = 0, timeAfter = 0;

			while (!game.isOver()) {
				timeBefore = System.currentTimeMillis();

				if (!this.pause) {
					Command actualCommand = control.getCommand();
					if (actualCommand.pause) {
						pauseTheGame();
					}
					game.evolve(actualCommand, updateTime);
					gui.draw();
				}

				timeAfter = System.currentTimeMillis();
				long elapsedTime = (timeAfter - timeBefore);
				long sleepDuration = updateTime - elapsedTime;

				if (sleepDuration > 0) {
					try {
						Thread.sleep(sleepDuration);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	private void pauseTheGame() {
		this.gameThread.pause = true;
		this.showState("PAUSE");
	}

	public void setUpdateTime(int updateTime) {
		this.updateTime = updateTime;
	}

	public void addState(JPanel panel, String id) {
		this.gui.addState(panel, id);
	}

	public void showState(String id) {
		this.gui.showState(id);
	}

	public void unpause() {
		this.gameThread.unpause();
		this.gui.activerControleur();
		this.showState("GAME");
	}

	public boolean isPaused() {
		return this.gameThread.isPaused();
	}

	public GraficInterface getGUI() {
		return this.gui;
	}

	public Game getGame() {
		return this.game;
	}
}
