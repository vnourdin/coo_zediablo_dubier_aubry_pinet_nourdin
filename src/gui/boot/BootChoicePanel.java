package gui.boot;

import editor.EditorFrame;
import gameEngine.GameEngine;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class BootChoicePanel extends JPanel implements ActionListener{

	private JButton launchGameChoice, editorChoice;
	private GameEngine gameEngine;

	public BootChoicePanel(GameEngine gameEngine)
	{
		super(new GridBagLayout());
		this.gameEngine = gameEngine;
		
		this.launchGameChoice = new GameChoiceButton();
		this.editorChoice = new EditorChoiceButton();

		this.launchGameChoice.addActionListener(this);
		this.editorChoice.addActionListener(this);
		
		this.add(this.launchGameChoice, new GridBagConstraints(0,0, 1,0, 0,0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
		this.add(this.editorChoice, new GridBagConstraints(1,1, 0,0, 0,0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource()==this.launchGameChoice)
		{
			gameEngine.showState("MENU");
		}

		if(e.getSource()==this.editorChoice)
		{
			new EditorFrame();
			this.gameEngine.getGUI().dispose();
		}		
	}
}