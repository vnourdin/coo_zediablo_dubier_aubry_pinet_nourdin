package items;

import entity.Character;
import myGame.RessourceLoader;

public class PotionToHeal extends Item{

	public PotionToHeal(){
		super(RessourceLoader.getPotionToHealSprite());
	}
	
	public String getDescription() {
		return "Potion de soin rend 20 point de vie au personnage";
	}

	public boolean use(Character user) {
		user.cure(20);
		return true;
	}

	@Override
	public String getName() {
		return Item.POTION_HEAL;
	}
}