package entity;

import behaviours.FightWithBow;
import behaviours.FightWithSword;
import environment.Labyrinth;
import graphicEngine.CharacterSprite;
import myGame.RessourceLoader;

public class ArcherSkeleton extends Monster{

	private int arrowsStock;
	
	public ArcherSkeleton(Labyrinth laby){
		super(RessourceLoader.getArcherBowSprite(),laby);
		this.arrowsStock = 3;
		this.setLife(20);
		this.attackSpeed = 500;
		this.range = 6;
		this.damages = 10;
		
		this.fight = new FightWithBow((CharacterSprite)this.sprite, this);
	}
	
	@Override
	public void attack(Character charact) {
		if(this.getArrowsStock() > 0){
			this.attackWithBow(charact);
			if(this.getArrowsStock()==0)
				this.prepareKnifeAttack();
		}
		else
			super.attack(charact);
	}
	
	public int getArrowsStock(){
		return this.arrowsStock;
	}
	
	private void attackWithBow(Character charact)
	{
		this.arrowsStock--;
		super.attack(charact);
	}
	
	private void prepareKnifeAttack()
	{
		this.range = 1;
		this.damages = 5;
		this.setSprite(RessourceLoader.getArcherKnifeSprite());
		this.fight = new FightWithSword((CharacterSprite)this.sprite);
	}

	@Override
	public String monsterType() {
		return Monster.ARCHER_SKELETON;
	}

	@Override
	public String xmlType() {
		return "ArcherSkeleton";
	}
}
