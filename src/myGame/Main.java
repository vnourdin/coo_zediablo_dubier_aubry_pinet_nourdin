package myGame;

import gameEngine.GameEngine;
import gui.MyGameDraw;
import gui.boot.BootChoicePanel;
import gui.launchGame.LaunchGamePanel;
import gui.pause.PausePanel;

public class Main {

	public static MyGame game;
	public static GameEngine gameEngine;

	public static void main(String[] args) throws InterruptedException {
		RessourceLoader.loadAll();
		RessourceLoader.launchMusic();

		game = new MyGame();
		MyGameDraw gameDraw = new MyGameDraw(game);
		gameEngine = new GameEngine(game, gameDraw, MyGameDraw.WIDTH,
				MyGameDraw.HEIGHT);

		BootChoicePanel boot = new BootChoicePanel(gameEngine);
		LaunchGamePanel menu = new LaunchGamePanel(gameEngine);
		PausePanel pause = new PausePanel(gameEngine);

		gameEngine.addState(menu, "MENU");
		gameEngine.addState(boot, "BOOT");
		gameEngine.addState(pause, "PAUSE");

		gameEngine.showState("BOOT");
	}
}