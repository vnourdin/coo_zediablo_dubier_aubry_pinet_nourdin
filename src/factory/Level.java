package factory;

import entity.Monster;
import entity.Player;
import environment.Labyrinth;
import items.Item;

import java.util.ArrayList;

public class Level {

	public Player player;
	public Labyrinth labyrinth;
	public ArrayList<Monster> monsters;
	public ArrayList<Item> items;
	
	public Level(Player player){
		this.player = player;
	}
}