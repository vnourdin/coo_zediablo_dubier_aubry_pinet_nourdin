package factory;

import items.Item;
import items.PotionToHeal;
import items.PotionToIncreaseSpeed;
import items.PotionToRegenerate;

import java.io.IOException;

import entity.ArcherSkeleton;
import entity.EnemyMage;
import entity.Monster;
import entity.OrcWarrior;
import environment.Tile;
import environment.Labyrinth;
import environment.Wall;
import environment.Trap;
import environment.Stairs;

public abstract class LabyrinthFactory {

	protected Labyrinth labyrinthToBuild;
	
	public void addCaseToLabyrinthe(Tile tile){
		if(tile.getID()==Tile.STAIRS)
			this.labyrinthToBuild.addStairs((Stairs) tile);
		else if(tile.getID()==Tile.TRAP)
			this.labyrinthToBuild.addTrap((Trap)tile);
		else
			this.labyrinthToBuild.setTile(tile);
	}
	
	public abstract Labyrinth generateLabyrinth() throws IOException;
	
	public Tile createCaseFromId(int x,int y,int id){
		switch(id){
		case Tile.EMPTY:
			return new Tile(x,y);
		case Tile.WALL:
			return new Wall(x,y);
		case Tile.STAIRS:
			return new Stairs(x,y);
		case Tile.TRAP:
			return new Trap(x,y);
		default:
			return new Tile(x,y);
		}
	}
	
	public Monster createMonsterFromType(String monsterType){
		if(monsterType == Monster.ORC_WARRIOR){
			return new OrcWarrior(null);
		}else if(monsterType == Monster.ARCHER_SKELETON){
			return new ArcherSkeleton(null);
		}else if(monsterType == Monster.ENEMY_MAGE){
			return new EnemyMage(null);
		}else{
			return null;
		}
	}
	
	public Item createItemFromName(String name){
		if(name == Item.POTION_HEAL){
			return new PotionToHeal();
		}else if(name == Item.POTION_INCREASE_SPEED){
			return new PotionToIncreaseSpeed();
		}else if(name == Item.POTION_REGENERATE){
			return new PotionToRegenerate();
		}else{
			return null;
		}
	}
}
