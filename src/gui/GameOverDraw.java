package gui;

import graphicEngine.Drawable;

import java.awt.Font;
import java.awt.Graphics2D;

public class GameOverDraw implements Drawable{

	private GuiText gtOver;
	
	public GameOverDraw()
	{
		this.gtOver = new GuiText(336, 336);
		this.gtOver.setText("Game Over !");
		this.gtOver.setFont(this.gtOver.getFont(), 100, Font.BOLD);
	}
	
	@Override
	public void draw(Graphics2D image) {
		this.gtOver.draw(image);
	}
}