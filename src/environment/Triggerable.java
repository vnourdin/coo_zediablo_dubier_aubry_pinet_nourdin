package environment;

import entity.Character;

public interface Triggerable {

	public void triggerOnCharacter(Character charact);
}