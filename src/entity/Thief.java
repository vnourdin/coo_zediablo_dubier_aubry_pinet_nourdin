package entity;

import items.Inventory;
import behaviours.FightWithSword;
import myGame.RessourceLoader;
import environment.Labyrinth;
import graphicEngine.CharacterSprite;

public class Thief extends Player {

	public Thief(Labyrinth laby) {
		super(RessourceLoader.getThiefSprite(),laby);
		this.fight = new FightWithSword((CharacterSprite) this.sprite);
		this.damages = 10;
		this.attackSpeed = 600;
		this.movingSpeed = 10;
		this.inventoryMaxSize = 15;
		this.inventory = new Inventory(this.inventoryMaxSize);
	}
	
	public Thief(String name, Labyrinth laby) {
		super(RessourceLoader.getThiefSprite(), name, laby);
		this.fight = new FightWithSword((CharacterSprite) this.sprite);
		this.damages = 10;
		this.attackSpeed = 600;
		this.movingSpeed = 10;
		this.inventoryMaxSize = 15;
		this.inventory = new Inventory(this.inventoryMaxSize);
	}
}