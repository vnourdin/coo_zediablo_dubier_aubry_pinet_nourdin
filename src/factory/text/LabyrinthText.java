package factory.text;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import environment.EmptyLabyrinth;
import environment.Labyrinth;
import factory.LabyrinthFactory;

public class LabyrinthText extends LabyrinthFactory{

	private String path_to_txt_file;
	
	public LabyrinthText(String path_to_txt_file){
		this.path_to_txt_file = path_to_txt_file;
	}
	
	@Override
	public Labyrinth generateLabyrinth() throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(path_to_txt_file)));
		String textLine = in.readLine();
		if(textLine!=null){
			// On lit le nom de la carte
			String labyrintheName = textLine;
			textLine = in.readLine();
			// On lit la definition de la carte
			String [] labyrinthDefinition = textLine.trim().split("x");
			int line = Integer.parseInt(labyrinthDefinition[0]);
			int column = Integer.parseInt(labyrinthDefinition[1]);
			
			this.labyrinthToBuild = new EmptyLabyrinth(line,column);
			this.labyrinthToBuild.setName(labyrintheName);

			for(int i=0;i<line;i++){
				// Pour chaque case de la matrice on lit son contenu (case vide, mur ou teleporteur)
				String data_line = in.readLine();
				String [] data_colonne = data_line.trim().split(":");
				if(data_colonne.length==column){
					for(int j=0;j<column;j++){
						int id = 0;
						try{
							id = Integer.parseInt(data_colonne[j]);
						}catch(NumberFormatException e){
							System.err.println("Numéro de case invalide (case par default vide): "+id);
						}
						addCaseToLabyrinthe(createCaseFromId(j,i,id));
					}
				}
			}
		}
		in.close();
		return this.labyrinthToBuild;
	}
}
