package gui;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import entity.Player;
import graphicEngine.Drawable;
import myGame.RessourceLoader;

/**
 * Classe qui sert a dessiner les coeurs du HUD.
 */
public class GuiLife implements Drawable{

	private Player player;
	private int x,y;
	
	public GuiLife(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	@Override
	public void draw(Graphics2D g2d) {
		AffineTransform trans = g2d.getTransform();
		g2d.translate(x, y);
		for(int i=0; i<this.player.getMaxLife()/10; i++)
		{
			if(i<this.player.getLife()/10)
				g2d.drawImage(RessourceLoader.heart, RessourceLoader.heart.getWidth()*(i%8), RessourceLoader.heart.getHeight()*(i/8), null);
			else
				g2d.drawImage(RessourceLoader.emptyHeart, RessourceLoader.emptyHeart.getWidth()*(i%8), RessourceLoader.emptyHeart.getHeight()*(i/8), null);
		}
		g2d.setTransform(trans);
	}
	
	public void setPlayer(Player player){
		this.player = player;
	}
	
	public int getHeightUsed()
	{
		return (this.y+((this.player.getMaxLife()/10-1)/8*25)+50);
	}
}