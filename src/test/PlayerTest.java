package test;
import myGame.RessourceLoader;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import entity.Player;
import entity.MageWoman;
import environment.Labyrinth;
import gameEngine.Command;

public class PlayerTest{

	Labyrinth lab;
	
	@Before
	public void setUp(){
		try{
			RessourceLoader.loadAll();
		}catch(Exception e){
			e.printStackTrace();
		}
		lab = new Labyrinth();
	}
	
	@Test
	public void isNewPlayerAlive(){
		Player j3 = new MageWoman(lab);
		
		assertEquals("Should be alive", true, !j3.isDeath());
	}
	
	@Test
	public void isDeadPlayerDead(){
		Player j4 = new MageWoman(lab);
		
		j4.die();
		
		assertEquals("Should be dead", true, j4.isDeath());
	}
	
	@Test
	public void isPlayerMovedToRight(){
		Player j5 = new MageWoman(lab);
		Command right = new Command();
		int firstPosX = j5.x;
		
		right.right = true;
		j5.setAction(right);
		j5.update(0);
		
		assertEquals("Should be moved by 1 at right (so x++)", firstPosX+1, j5.x);
	}
	
	@Test
	public void isPlayerMovedToLeft(){
		Player j6 = new MageWoman(lab);
		Command left = new Command();
		int firstPosX = j6.x;
		
		left.left = true;
		j6.setAction(left);
		j6.update(0);
		
		assertEquals("Should be moved by 1 at left (so x--)", firstPosX-1, j6.x);
	}
	
	@Test
	public void isPlayerMovedDown(){
		Player j7 = new MageWoman(lab);
		Command down = new Command();
		int firstPosY = j7.y;
		
		down.down = true;
		j7.setAction(down);
		j7.update(0);
		
		assertEquals("Should be moved by 1 down (so y++)", firstPosY+1 ,j7.y);
	}
	
	@Test
	public void isPlayerMovedUp(){
		Player j8 = new MageWoman(lab);
		Command up = new Command();
		int firstPosY = j8.y;
		
		up.up = true;
		j8.setAction(up);
		j8.update(0);
		
		assertEquals("Should be moved by 1 up (so y--)", firstPosY+1, j8.y);
	}
	
	@Test
	public void doesPlayerRespectLeftLimit(){
		Player j9 = new MageWoman(lab);
		Command left = new Command();
		j9.setPosition(0,0);
		
		left.left = true;
		j9.setAction(left);
		j9.update(0);
		
		assertEquals("Should rest 0", 0, j9.x);
	}
	
	@Test
	public void doesPlayerRespectRightLimit(){
		Player j10 = new MageWoman(lab);
		Command right = new Command();
		j10.setPosition(15,0);
		
		right.right = true;
		j10.setAction(right);
		j10.update(0);
		
		assertEquals("Should rest 15", 15, j10.x);
	}
	
	@Test
	public void doesPlayerRespectUpLimit(){
		Player j11 = new MageWoman(lab);
		Command up = new Command();
		j11.setPosition(0,0);
		
		up.up = true;
		j11.setAction(up);
		j11.update(0);
		
		assertEquals("Should rest 0", 0, j11.y);
	}
	
	@Test
	public void doesPlayerRespectDownLimit(){
		Player j12 = new MageWoman(lab);
		Command down = new Command();
		j12.setPosition(0,15);
		
		down.down = true;
		j12.setAction(down);
		j12.update(0);
		
		assertEquals("Should rest 15", 15, j12.y);
	}
	
	@Test
	public void testAnimation(){
		Player j1 = new MageWoman(lab);
		Command down = new Command();
		j1.setPosition(0,0);
		
		down.down = true;
		j1.setAction(down);
		for(int i=0;i<7;i++)
			j1.update(0);
		
		assertEquals("Should be 1", 1, j1.y);
		assertEquals("Should be 42", 42, j1.getSprite().getPosY());
	}
}
