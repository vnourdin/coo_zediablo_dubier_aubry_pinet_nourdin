package gameEngine;

public interface Game {

	public void evolve(Command commandeUser, long delta_time);

	public boolean isOver();
}