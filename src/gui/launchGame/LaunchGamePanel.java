package gui.launchGame;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import entity.*;
import factory.Level;
import factory.xml.XMLLevel;
import gameEngine.GameEngine;
import myGame.Main;
import myGame.RessourceLoader;

public class LaunchGamePanel extends JPanel implements ActionListener,KeyListener {
	private JButton playButton;
	private JTextField nameSelection;
	private JComboBox spriteSelection;
	private Player player;
	private GameEngine gameEngine;

	public LaunchGamePanel(GameEngine engine) {
		this.gameEngine = engine;

		this.setLayout(new BorderLayout());

		JPanel up = new JPanel();
		JLabel nameSelectionDescription = new NameSelectionDescription();
		up.add(nameSelectionDescription);

		this.nameSelection = new NameSelectionField(this);
		up.add(this.nameSelection);

		this.playButton = new PlayButton(this);
		up.add(this.playButton);
		up.setOpaque(false);

		JPanel center = new JPanel();
		JPanel centerUp = new JPanel();
		JLabel characterSelectionDescription = new JLabel(
				"Choix du personnage : ");
		try {
			characterSelectionDescription.setFont(Font.createFont(
					Font.TRUETYPE_FONT,
					new File("Ressources/Fonts/AppleBerry.ttf")).deriveFont(
					Font.PLAIN, 20));
		} catch (FontFormatException | IOException e) {
			e.printStackTrace();
		}
		characterSelectionDescription.setOpaque(false);

		centerUp.add(characterSelectionDescription);
		this.spriteSelection = new SpriteSelecter(this);
		centerUp.add(this.spriteSelection);
		centerUp.setOpaque(false);

		center.setLayout(new BorderLayout());
		center.add(centerUp, BorderLayout.NORTH);
		center.setOpaque(false);

		JPanel total = new JPanel();
		total.setLayout(new BorderLayout());
		total.setPreferredSize(new Dimension(200, 600));
		total.add(up, BorderLayout.NORTH);
		total.add(center, BorderLayout.CENTER);
		total.setOpaque(false);

		this.add(total, BorderLayout.SOUTH);
		this.player = new MageWoman(null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.playButton) {
			this.player.changeName(this.nameSelection.getText());
			Main.game.setPlayer(player);

			try {
				Level test = new XMLLevel(player,
						"Ressources/Levels/XMLLevels/1.xml").generateLevel();
				Main.game.loadLevel(test);
				this.gameEngine.showState("GAME");
				this.gameEngine.startTheGame();
			} catch (IOException | InterruptedException e1) {
				e1.printStackTrace();
			}
		}
		if (e.getSource() == this.spriteSelection) {
			switch (this.spriteSelection.getSelectedIndex()) {
			case 0:
				this.player = new MageWoman(null);
				break;
			case 1:
				this.player = new Warrior(null);
				break;
			case 2:
				this.player = new MageMan(null);
				break;
			case 3:
				this.player = new Thief(null);
				break;
			}
			System.out.println(this.player);
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(RessourceLoader.gameLaunchBackground, 0, 0,
				this.getWidth(), this.getHeight(), this);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER)
			this.playButton.doClick();
	}

	@Override
	public void keyReleased(KeyEvent arg0) {}

	@Override
	public void keyTyped(KeyEvent arg0) {}
}