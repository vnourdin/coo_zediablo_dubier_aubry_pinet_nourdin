package gui.pause;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import entity.MageMan;
import entity.MageWoman;
import entity.Player;
import entity.Thief;
import entity.Warrior;
import gameEngine.GameEngine;
import graphicEngine.Drawable;
import gui.GuiText;
import myGame.MyGame;
import myGame.RessourceLoader;

public class SpritePanel extends JPanel implements Drawable {

	private Player player;
	private GameEngine gameEngine;
	private GuiText playerName;

	public SpritePanel(GameEngine gameEngine) {
		this.setGameEngine(gameEngine);
		this.actualisePlayer();
		this.initialisePlayerName();
		this.setPreferredSize(new Dimension(this.gameEngine.getGUI().getWidth() / 2, 4*this.gameEngine.getGUI().getHeight() / 5));
	}

	private void setGameEngine(GameEngine gameEngine) {
		this.gameEngine = gameEngine;
	}

	private void actualisePlayer() {
		this.player = ((MyGame) (this.gameEngine.getGame())).getPlayer();
	}

	private void initialisePlayerName() {
		this.playerName = new GuiText(this.getPreferredSize().width / 2, this.getPreferredSize().height - 30);
	}

	private void actualisePlayerName() {
		this.playerName = new GuiText((this.getPreferredSize().width / 2) - (this.player.getName().length() * 15 / 2),
				this.getPreferredSize().height - 60);
		this.playerName.setText(this.player.getName());
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		this.actualisePlayer();
		this.actualisePlayerName();

		g.drawImage(this.determineImageToUse(),
				this.getPreferredSize().width / 2 - this.determineImageToUse().getWidth(null) / 2,
				this.getPreferredSize().height / 2 - this.determineImageToUse().getHeight(null) / 2 - 30, null);
		this.draw((Graphics2D) g);
	}

	private Image determineImageToUse() {
		if (this.player instanceof MageWoman)
			return RessourceLoader.heromw_Image;

		else if (this.player instanceof Warrior)
			return RessourceLoader.herow_Image;

		else if (this.player instanceof MageMan)
			return RessourceLoader.heromm_Image;

		else if (this.player instanceof Thief)
			return RessourceLoader.herot_Image;

		else
			return RessourceLoader.heromw_Image;
	}

	@Override
	public void draw(Graphics2D g) {
		this.playerName.draw(g);
	}
}
