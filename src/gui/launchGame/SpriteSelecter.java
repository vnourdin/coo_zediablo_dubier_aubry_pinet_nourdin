package gui.launchGame;

import java.awt.event.ActionListener;

import javax.swing.JComboBox;

public class SpriteSelecter extends JComboBox {

	public SpriteSelecter(ActionListener listener) {
		super(new String[] { "Magicienne", "Guerrier", "Magicien", "Voleur" });
		this.addActionListener(listener);
	}
}