# Projet Zeldiablo #
* * *


## Participants ##


* Tomtombinary : Thomas DUBIER
* Cypaubr : Cyprien AUBRY
* kiraboss : Valentin NOURDIN
* ChaosPrime : Arnaud PINET

  S2-D DUT Informatique à Nancy-Charlemagne

## Description ##

  Ce Programme est un jeu de type rogue-like,
très simple. Le but est de ne pas mourir face aux ennemis
générés aléatoirement.

  Les cartes sont choisies au hasard parmis des fichiers texte
ainsi que les patterns des murs et des sols ce qui fait un nombre
de combinaisons possibles très élevé.

## Sources

Les patterns des sol proviennent de http://bgrepeat.com et ceux des murs de https://cs169game.googlecode.com/svn/tags/Release-1/sprites/wall/