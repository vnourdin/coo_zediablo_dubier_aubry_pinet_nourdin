package graphicEngine;

import java.awt.image.BufferedImage;

/**
 * Sprite annimees
 */
public class AnimatedSprite extends Sprite{

	private long elapsedTime;
	private int nextFrame;
	private BufferedImage[] frames;
	private boolean isLoop;
	private boolean isStarted;
	private long timeBetweenTwoFrames;
	
	/**
	 * Construit un sprite anime
	 * @param source
	 *  image source (Spritesheet)
	 * @param nFrame
	 *  nombre d'images
	 * @param offsetX
	 *  decalage x en pixels pour le decoupage
	 * @param offsetY
	 *  decalage y en pixels pour le decoupage
	 * @param ligne
	 *  nombre d'image sur la hauteur
	 * @param colonne
	 *  nombre d'image sur la largeur
	 */
	public AnimatedSprite(BufferedImage source,int nFrame,int offsetX,int offsetY,int ligne,int colonne) {
		super(source);
		this.frames = new BufferedImage[nFrame];
		this.isLoop  = false;
		this.elapsedTime = 0;
		this.timeBetweenTwoFrames = 25;
		this.isStarted = false;
		this.cut(offsetX,offsetY,ligne,colonne);
		this.current = this.frames[0];
	}
	
	private void cut(int offsetX, int offsetY, int line, int column){
		int frame_w = source.getWidth()/column;
		int frame_h = source.getHeight()/line;
		for(int i=0;i<this.frames.length;i++){
			int posX = offsetX+frame_w*(i%column);
			int posY = offsetY+frame_h*(i/column);
			this.frames[i] = source.getSubimage(posX,posY,frame_w,frame_h);
		}
	}

	@Override
	public void update(long delta_time) {
		super.update(delta_time);

		if (this.isOkToChangeFrame(delta_time))
			this.changeFrame();
				
		this.chooseTheRest();
	}
	
	private boolean isOkToChangeFrame(long delta_time)
	{
		if(this.isStarted)
		{
			this.elapsedTime += delta_time;
			return (this.elapsedTime > this.timeBetweenTwoFrames);
		}
		else
			return false;
	}
	
	private void changeFrame()
	{
		this.current = this.frames[this.nextFrame];
		this.elapsedTime = 0;
		this.nextFrame++;
	}
	
	private void chooseTheRest()
	{
		if(this.isStarted)
		{
			if(this.isLoop)
				this.nextFrame = this.nextFrame%this.frames.length;
			else if(this.nextFrame >= this.frames.length)
				this.isStarted = false;
		}
	}
	
	public void start(){
		this.nextFrame = 0;
		this.elapsedTime = 0;
		this.isStarted = true;
	}

	public BufferedImage getCurrent(){
		return this.current;
	}
	
	public boolean isLoop() {
		return isLoop;
	}

	public void setLoop(boolean loop) {
		this.isLoop = loop;
	}

	public long getTimeWait() {
		return timeBetweenTwoFrames;
	}

	public void setTimeWait(long timeWait) {
		this.timeBetweenTwoFrames = timeWait;
	}

	public boolean isStarted() {
		return isStarted;
	}
}
