package graphicEngine;

import gameEngine.Controller;

import java.awt.CardLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class GraficInterface extends JFrame {

	private JPanel contentPanel;
	private GamePannel gamePanel;
	private Controller cotroller;

	private CardLayout states;

	/**
	 * la construction de l'interface graphique
	 * - construit la JFrame
	 * - construit les Attributs
	 * 
	 * @param drawableToUse l'afficheur a utiliser dans le moteur
	 */
	public GraficInterface(Drawable drawableToUse, int width, int height)
	{
		super("Zeldiablo");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocation(200, 60);

		this.contentPanel = new JPanel();
		this.contentPanel.setPreferredSize(new Dimension(width,height));
		this.states = new CardLayout();
		this.contentPanel.setLayout(states);

		// creation du panel qui gere le jeu
		this.gamePanel = new GamePannel(width,height,drawableToUse);
		this.contentPanel.add(gamePanel,"GAME");

		//ajout du controleur
		Controller controlleurGraph = new Controller();
		this.cotroller = controlleurGraph;
		this.addKeyListener(controlleurGraph);

		this.setContentPane(this.contentPanel);
		this.pack();
		this.setResizable(false);
		this.setVisible(true);
	}

	public void addState(JPanel panel,String id){
		this.contentPanel.add(panel,id);
	}

	public void showState(String id){
		this.states.show(this.contentPanel,id);
	}

	public Controller getControleur() {
		return cotroller;
	}

	public void draw() {
		this.gamePanel.drawTheGame();	
	}

	public void activerControleur(){
		this.cotroller.reset();
		this.requestFocusInWindow();
	}
}
