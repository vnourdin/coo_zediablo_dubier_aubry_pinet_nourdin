package editor;

import java.awt.GridLayout;

import items.Item;
import items.PotionToHeal;
import items.PotionToIncreaseSpeed;
import items.PotionToRegenerate;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;

public class ItemsPanel extends JPanel{

	private static Item [] allItems = {
		new PotionToHeal(),
		new PotionToIncreaseSpeed(),
		new PotionToRegenerate()
	};

	public ItemsPanel(EntityController controller,LabyrinthPanel labyPanel){
		ButtonGroup group = new ButtonGroup();
		this.setLayout(new GridLayout(2,2));
		for(Item i : allItems){
			EntityButton button_item = new EntityButton(controller,i);
			i.addObserver(labyPanel);
			group.add(button_item);
			this.add(button_item);
		}
	}
}