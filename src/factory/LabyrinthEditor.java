package factory;

import items.Item;

import java.io.IOException;

import entity.Monster;
import environment.Labyrinth;

public class LabyrinthEditor extends LabyrinthFactory{

	public LabyrinthEditor(Labyrinth labyrinth){
		this.labyrinthToBuild = labyrinth;
	}
	
	@Override
	public Labyrinth generateLabyrinth() throws IOException {
		return this.labyrinthToBuild;
	}

	public void addMonsterToLabyrinth(Monster monsterToAdd) {
		if(!this.labyrinthToBuild.isSolid(monsterToAdd.y,monsterToAdd.x)){
			this.labyrinthToBuild.addCharacter(monsterToAdd);
		}
	}
	
	public void addItemToLabyrinth(Item item){
		this.labyrinthToBuild.addItem(item);
	}

}
