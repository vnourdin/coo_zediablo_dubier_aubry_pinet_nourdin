package editor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

public class EditorMenu extends JMenuBar implements ActionListener{

	private LevelExporter export;
	private JMenu fileMenu;
	private JMenuItem load;
	private JMenuItem save;

	public EditorMenu(LevelExporter export){
		this.export = export;
		this.fileMenu = new JMenu("Fichier");
		this.load = new JMenuItem("Charger");
		this.save = new JMenuItem("Sauvegarder");

		this.load.addActionListener(this);
		this.save.addActionListener(this);

		this.fileMenu.add(load);
		this.fileMenu.add(save);
		add(fileMenu);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource()==this.save){
			JFileChooser fileChooser = new JFileChooser();
			int retval = fileChooser.showSaveDialog(null);
			if(retval == JFileChooser.APPROVE_OPTION)
				try {
					this.export.export(fileChooser.getSelectedFile());
				} catch (ParserConfigurationException | TransformerException e) {
					e.printStackTrace();
				}
		}
	}
}