package gui.launchGame;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.File;
import java.io.IOException;

import javax.swing.JLabel;

public class NameSelectionDescription extends JLabel {
	
	public NameSelectionDescription()
	{
		super("Nom du joueur : ");
		try {
			this.setFont(Font.createFont(Font.TRUETYPE_FONT, new File("Ressources/Fonts/AppleBerry.ttf")).deriveFont(Font.PLAIN, 20));
		}
		catch (FontFormatException | IOException e) {e.printStackTrace();}
	}
}