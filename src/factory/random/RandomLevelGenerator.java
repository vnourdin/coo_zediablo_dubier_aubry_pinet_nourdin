package factory.random;

import java.io.IOException;

import entity.Player;
import factory.Level;
import factory.LevelFactory;

public class RandomLevelGenerator extends LevelFactory{

	public RandomLevelGenerator(Player player) {
		super(player);
	}
	
	@Override
	public Level generateLevel() throws IOException{
		this.level.player.setPosition(7,7);
		return super.generateLevel();
	}

	@Override
	public void prepareFactory() {
		this.labyrinth_factory = new LabyrinthTextRandom();
		this.monsters_factory  = new MonstersRandom(level.player);
		this.item_factory      = new ItemsRandom();
	}

}
