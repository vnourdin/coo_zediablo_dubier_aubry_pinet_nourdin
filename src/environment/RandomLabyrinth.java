package environment;

public class RandomLabyrinth extends Labyrinth{

	public RandomLabyrinth()
	{
		this.name = "Labyrinthe aléatoire";
		this.column = 16;
		this.line = 16;
		this.contents = new Tile[this.line][this.column];
		for(int i=0;i<this.line;i++){
			for(int j=0; j<this.column; j++){
				if((int)(Math.random()*2)==0)
					this.contents[i][j] = new Wall(j,i);
				else
					this.contents[i][j] = new Tile(j,i);
			}
		}
		this.initialiseLists();
	}
}