package editor;

import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;

import environment.Stairs;
import environment.Tile;
import environment.Trap;
import environment.Wall;

public class TilesPanel extends JPanel{

	private static Tile[] allTiles = new Tile[]{
		new Tile(0,0),
		new Trap(0,0),
		new Wall(0,0),
		new Stairs(0,0)
	};

	public TilesPanel(EntityController controller,LabyrinthPanel labyPanel){
		ButtonGroup group = new ButtonGroup();
		this.setLayout(new GridLayout(2,2));
		for(Tile t : allTiles){
			EntityButton button_tile = new EntityButton(controller,t);
			t.addObserver(labyPanel);
			group.add(button_tile);
			this.add(button_tile);
		}
	}
}