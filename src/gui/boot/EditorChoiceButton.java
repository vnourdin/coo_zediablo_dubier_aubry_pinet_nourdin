package gui.boot;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import myGame.RessourceLoader;

public class EditorChoiceButton extends JButton{
	
	public EditorChoiceButton()
	{
		super(new ImageIcon(RessourceLoader.editorButton));
		this.setPressedIcon(new ImageIcon(RessourceLoader.editorButtonPressed));
		this.setRolloverIcon(new ImageIcon(RessourceLoader.editorButtonRollover));
		this.setContentAreaFilled(false);
		this.setBorderPainted(false);
		this.setFocusPainted(false);
	}
}
