package gameEngine;

/**
 * Interface represantant tout objet du jeu qui evolue au cours du temps
 */
public interface Updateable {

	/**
	 * Methode d'update de l'objet
	 * @param delta_time
	 *  temps ecoule depuis la derniere frame
	 */
	public void update(long delta_time);
}
