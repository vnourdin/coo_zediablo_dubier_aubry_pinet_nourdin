package behaviours;

import entity.Character;
import environment.Labyrinth;
import gameEngine.Command;
import graphicEngine.CharacterSprite;
import gui.MyGameDraw;

public class Walking extends Moving{

	public Walking(Character pers) {
		super(pers);
	}

	@Override
	public void move(Command action){
		Labyrinth labyrinthe = pers.getLabyrinth();
		boolean movePlayer = false;
		if (action.left)
		{
			movePlayer = true;
			pers.orientate(CharacterSprite.LEFT);
			if(!labyrinthe.isSolid(pers.y, pers.x-1))
				this.pers.x--;
		}

		if (action.right && !movePlayer)
		{
			movePlayer = true;
			pers.orientate(CharacterSprite.RIGHT);
			if(!labyrinthe.isSolid(pers.y, pers.x+1))
				pers.x++;
		}

		if (action.up && !movePlayer)
		{
			movePlayer = true;
			pers.orientate(CharacterSprite.UP);
			if(!labyrinthe.isSolid(pers.y-1,pers.x))
				pers.y--;
		}

		if (action.down && !movePlayer)
		{
			movePlayer = true;
			pers.orientate(CharacterSprite.DOWN);
			if(!labyrinthe.isSolid(pers.y+1,pers.x))
				pers.y++;
		}
		
		if(movePlayer){
			/* On demarre une translation sur 14 frame
			 * pour passez a la case suivante
			 */
			pers.getSprite().startTranslation(
					pers.x*MyGameDraw.TILE_SIZE+MyGameDraw.TILE_SIZE/2,
					pers.y*MyGameDraw.TILE_SIZE+MyGameDraw.TILE_SIZE/2,
					pers.getMovingSpeed());
			/* On lance l'animation de déplacement */
			pers.getCharacterSprite().play(CharacterSprite.MOVING,pers.getOrientation());
			labyrinthe.triggerOnCharacter(pers);
		}
	}
}
