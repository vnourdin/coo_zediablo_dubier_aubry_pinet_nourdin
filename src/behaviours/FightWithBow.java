package behaviours;

import entity.Arrow;
import entity.Character;
import gameEngine.Updateable;
import graphicEngine.Drawable;
import graphicEngine.CharacterSprite;
import myGame.Main;

public class FightWithBow extends Fight{

	private Character origin;
	
	public FightWithBow(CharacterSprite sprite,Character source) {
		super(sprite);
		this.origin = source;
	}
	
	@Override
	public void attack(Character caract, int orientation, int damages){
		sprite.play(CharacterSprite.BOW, orientation);
		Arrow arrow = new Arrow(origin);
		Main.game.waitForAdd((Updateable)arrow);
		Main.game.waitForAdd((Drawable)arrow);
	}
}