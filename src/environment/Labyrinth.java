package environment;

import items.Item;

import java.awt.Graphics2D;
import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import editor.Editable;
import entity.Character;
import gameEngine.Updateable;
import graphicEngine.Drawable;

public class Labyrinth extends Editable implements Drawable,Triggerable,Updateable{

	protected int column;
	protected int line;
	protected String name;
	protected Tile [][] contents;
	protected ArrayList<Character> characters;
	protected ArrayList<Item> items;
	protected ArrayList<Triggerable> triggerables;
	protected ArrayList<Stairs> stairs;
	protected ArrayList<Updateable> updatables;

	protected void initialiseLists()
	{
		this.triggerables = new ArrayList<Triggerable>();
		this.characters = new ArrayList<Character>();
		this.stairs = new ArrayList<Stairs>();
		this.items = new ArrayList<Item>();
		this.updatables = new ArrayList<Updateable>();
	}
	
	@Override
	public void draw(Graphics2D image) {
		for (int i=0; i<this.line;i++)
		{
			for(int j=0; j<this.column; j++)
				this.contents[i][j].draw(image);
		}
	}

	public boolean isSolid(int line, int column)
	{
		if(line<this.line && line>=0 && column<this.column && column >=0){
			if(this.contents[line][column].isSolid())
				return true;
			else if(this.getCharacterAt(line,column)!=null)
				return !this.getCharacterAt(line,column).isDeath();
			else
				return false;
		}
		else
			return true;
	}

	public void setTile(Tile tile)
	{
		if(this.isEditing())
			tile.startEditing();
		if(tile.getY()<this.line && tile.getY()>=0 && tile.getX()<this.column && tile.getX() >=0)
			this.contents[tile.getY()][tile.getX()] = tile;
	}

	public Character getCharacterAt(int i,int j){
		for(int k=0; k<this.characters.size(); k++){
			if((this.characters.get(k).x == j) && ((this.characters.get(k).y == i)) && !this.characters.get(k).isDeath()){
				return this.characters.get(k);
			}
		}
		return null;
	}

	public void addCharacter(Character charact){
		this.characters.add(charact);
	}

	public int getLine(){
		return this.line;
	}

	public int getColumn(){
		return this.column;
	}

	public String getName()
	{
		return this.name;
	}

	public void setName(String name){
		this.name = name;
	}

	@Override
	public void triggerOnCharacter(Character p) {
		for(Triggerable t: triggerables)
			t.triggerOnCharacter(p);
	}

	public void addTriggerable(Triggerable t){
		this.triggerables.add(t);
	}

	public ArrayList<Character> getAllCharacters(){
		return this.characters;
	}

	public ArrayList<Item> getAllItems(){
		return this.items;
	}

	public void addTrap(Trap trap) {
		setTile(trap);
		addTriggerable(trap);
		updatables.add(trap);
	}

	@Override
	public void update(long delta_time) {
		for(Updateable u : updatables){
			u.update(delta_time);
		}
	}

	public Element toXML(Document doc,String datasource,String datasourcetype){
		Element labyrinth_xml = doc.createElement("labyrinth");
		labyrinth_xml.setAttribute("line",Integer.toString(line));
		labyrinth_xml.setAttribute("column",Integer.toString(column));

		Element name = doc.createElement("name");
		name.setTextContent(this.name);

		Element datasource_xml = doc.createElement("datasource");
		datasource_xml.setAttribute("type",datasourcetype);
		datasource_xml.setTextContent(datasource);

		labyrinth_xml.appendChild(name);
		labyrinth_xml.appendChild(datasource_xml);
		return labyrinth_xml;
	}

	public String toTxtV2(){
		StringBuffer content = new StringBuffer();
		for(int i=0;i<this.line;i++){
			content.append(Integer.toString(this.contents[i][0].getID()));
			for(int j=1;j<this.column;j++){
				content.append(":"+Integer.toString(this.contents[i][j].getID()));
			}
			content.append('\n');
		}
		return content.toString();
	}

	@Override
	public String toString(){
		String ret = this.name+"\n";
		ret+= String.format("%dx%d\n",this.line,this.column);
		ret+= toTxtV2();
		return ret;
	}
	
	//--// Edition //--//
	
	@Override
	public void startEditing(){
		for(int i=0; i<this.line; i++){
			for(int j=0; j<this.column; j++){
				this.contents[i][j].startEditing();
			}
		}
		super.startEditing();
	}

	@Override
	public void stopEditing(){
		for(int i=0;i<this.line;i++){
			for(int j=0;j<this.column;j++){
				this.contents[i][j].stopEditing();
			}
		}
		super.stopEditing();
	}
	
	//--// Items //--//
	

	public void addItem(Item item){
		this.items.add(item);
	}

	public void removeItem(Item item){
		this.items.remove(item);
	}

	public Item getItemAt(int x,int y){
		for(Item item : items){
			if(item.x==x && item.y==y)
				return item;
		}
		return null;
	}

	
	//--// Stairs //--//
	
	 
	public void addStairs(Stairs stairs){
		this.stairs.add(stairs);
		this.addTriggerable(stairs);
		this.setTile(stairs);
	}

	public void activateAllStrairs(){
		for(Stairs t : this.stairs){
			t.showStairs();
		}
	}
}