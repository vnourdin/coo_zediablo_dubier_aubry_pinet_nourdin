package gui;

import graphicEngine.Drawable;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

import myGame.Main;
import myGame.MyGame;

public class MyGameDraw implements Drawable {

	public final static int TILE_SIZE = 42;
	public final static int WIDTH = 16*TILE_SIZE+200;
	public final static int HEIGHT= 16*TILE_SIZE;
	
	private float scale = 1;
	private MyGame game;
	private HUD hud;

	public MyGameDraw(MyGame game) {
		this.game = game;
		this.hud = new HUD(this.game);
	}

	public void draw(Graphics2D g2d) {
		scale = game.getScale();
		g2d.setColor(Color.BLACK);
		g2d.fillRect(0,0,WIDTH,HEIGHT);
		ArrayList<Drawable> drawables = this.game.getDrawables();
		AffineTransform save = g2d.getTransform();
		
		//Recadrage de la camera
		//WIDTH - 200 (à cause de l'HUD)
		int tx = (int)(((WIDTH-200)/2-(this.game.getPlayer().getSprite().getPosX())*scale));
		int ty = (int)((HEIGHT/2-(this.game.getPlayer().getSprite().getPosY()*scale)));
		if(tx > 0)
			tx = 0;
		if(tx < -Main.game.getLabyrinth().getColumn()*TILE_SIZE - (WIDTH-200)/2)
			tx =  -Main.game.getLabyrinth().getColumn()*TILE_SIZE - (WIDTH-200)/2;
		if(ty > 0)
			ty = 0;
		if(ty < -Main.game.getLabyrinth().getLine()*TILE_SIZE - HEIGHT/2)
			ty = (int)(-Main.game.getLabyrinth().getLine()*TILE_SIZE - HEIGHT/2);
		
		g2d.translate(tx,ty);
		g2d.scale(scale,scale);
		for(Drawable drawable : drawables)
			drawable.draw(g2d);
		g2d.setTransform(save);
		this.hud.draw(g2d);
	}

	public MyGame getGame() {
		return this.game;
	}
}
