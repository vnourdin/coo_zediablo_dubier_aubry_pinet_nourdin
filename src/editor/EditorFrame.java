package editor;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import myGame.RessourceLoader;
import entity.Entity;
import environment.EmptyLabyrinth;
import environment.Labyrinth;
import environment.Wall;

public class EditorFrame extends JFrame{

	private LabyrinthPanel maze_panel;
	private ToolsPanel tools_panel;
	private EditorMenu menu_bar;
	
	private Labyrinth labtobuild;
	private Entity entitytobuild;
	
	public EditorFrame(){
		super("Editor");
		
		labtobuild = new EmptyLabyrinth(16,16);
		entitytobuild = new Wall(0,0);
		
		ZoomController zoom_controller = new ZoomController();
		EntityController entity_controller = new EntityController(entitytobuild,labtobuild);

		this.maze_panel  = new LabyrinthPanel(this);
		this.tools_panel = new ToolsPanel(entity_controller,this.maze_panel);
		this.menu_bar    = new EditorMenu(new LevelExporter(labtobuild));
		
		labtobuild.addObserver(this.maze_panel);
		entitytobuild.addObserver(this.maze_panel);
		
		this.maze_panel.addMouseWheelListener(zoom_controller);
		this.maze_panel.addMouseListener(entity_controller);
		this.maze_panel.addMouseMotionListener(entity_controller);
		
		JPanel content = new JPanel();
		content.setLayout(new BorderLayout());
		JScrollPane scrollPane = new JScrollPane(this.maze_panel);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

		scrollPane.setPreferredSize(new Dimension(800,600));
		content.add(scrollPane,BorderLayout.WEST);
		content.add(this.tools_panel);
		content.add(this.menu_bar,BorderLayout.NORTH);
		content.setPreferredSize(new Dimension(1200,600));
		
		this.setContentPane(content);
		this.pack();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		
		labtobuild.startEditing();
	}
	
	public static void main(String[] args) throws IOException {
		RessourceLoader.loadAll();
		new EditorFrame();
	}
}