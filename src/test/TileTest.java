package test;

import static org.junit.Assert.assertEquals;
import myGame.RessourceLoader;

import org.junit.Before;
import org.junit.Test;

import entity.Character;
import entity.MageWoman;
import environment.EmptyLabyrinth;
import environment.Tile;

public class TileTest {


	@Before
	public void setUp(){
		try{
			RessourceLoader.loadAll();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Test
	public void isNewTileEmpty(){
		Tile t1 = new Tile(0, 0);

		assertEquals("Should be empty", Tile.EMPTY, t1.getID());
	}

	@Test
	public void isNewTileNotSolid(){
		Tile t2 = new Tile(0,0);

		assertEquals("Shouldn't be solid", false, t2.isSolid());
	}

	@Test
	public void isPlayerOnTheSamePlace(){
		Tile t3 = new Tile(0,0);

		Character p = new MageWoman(new EmptyLabyrinth(2,2));
		p.setPosition(0,0);

		assertEquals("Devrait etre dessus",true,p.isOnTheSamePlace(t3));
	}
}