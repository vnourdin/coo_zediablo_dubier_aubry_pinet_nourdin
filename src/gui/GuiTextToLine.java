package gui;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

public class GuiTextToLine extends GuiText{

	public GuiTextToLine(int x, int y) {
		super(x, y);
	}

	@Override
	public void draw(Graphics2D g) {
		AffineTransform svg = g.getTransform();
		g.translate(x,y);
		g.setColor(Color.BLACK);
		g.setFont(this.font);
		for(int i=0; i<this.contents.length; i++)
		{
			g.drawString(this.contents[i], 15, i*40);
		}
		g.setTransform(svg);
	}

	public void setText(String s){
		if(s!=null)
			this.contents = s.split(" ");
	}
}
