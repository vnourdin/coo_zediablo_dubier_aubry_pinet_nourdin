package items;

import java.util.ArrayList;

import myGame.Main;
import entity.Character;
import environment.Labyrinth;
import gameEngine.Updateable;
import graphicEngine.Drawable;

public class Inventory {
	
	private ArrayList<Item> items;
	private int maxOfItems;
	
	public Inventory(int maxOfItems){
		this.maxOfItems = maxOfItems;
		this.items = new ArrayList<Item>(maxOfItems);
	}
	
	public boolean add(Item newItem){
		if(!this.isFull()){
			this.items.add(newItem);
			return true;
		}
		else
			return false;
	}
	
	/**
	 * Retire un item de l'inventaire
	 * @param index
	 *  index de l'item a retirer
	 */
	public void remove(int index){
		if(index>=0 && index<this.items.size())
			this.items.remove(index);
	}
	
	/**
	 * Lache un item dans le labyrinthe spécifier a une position donnée
	 * @param index
	 *  index de l'item a lacher
	 * @param x
	 *  position x de l'item
	 * @param y
	 *  position y de l'item
	 * @param lab
	 *  labyrinthe dans lequel lacher l'item
	 *  pour qu'il soit de nouveau ramasse
	 */
	public void dropItem(int index,int x,int y,Labyrinth lab){
		if(index>=0 && index<this.items.size()){
			Item item = this.items.get(index);
			item.setPosition(x, y);
			lab.addItem(item);
			Main.game.waitForAdd((Drawable)item);
			Main.game.waitForAdd((Updateable)item);
			this.items.remove(index);
		}
	}
	
	/**
	 * Utilise l'item à l'indice donné
	 * @param index
	 *  indice de l'item a utiliser
	 * @param user
	 *  utilisateur
	 */
	public void use(int index, Character user){
		if(index>=0 && index < this.items.size()){
			Item item = this.items.get(index);
			if(item.use(user)){
				this.remove(index);
			}
		}
	}
	
	public void destroyInventory(int x,int y, Labyrinth laby){
		for(int i=0;i<this.items.size();i++){
			Item item = this.items.get(i);
			item.setPosition(x, y);
			laby.addItem(item);
			Main.game.waitForAdd((Drawable)item);
			Main.game.waitForAdd((Updateable)item);
		}
		this.items.clear();
	}
	
	public boolean isFull(){
		return (this.items.size()==this.maxOfItems);
	}
	
	public ArrayList<Item> getAllItems(){
		return this.items;
	}
	
	public int size(){
		return this.items.size();
	}
	
	public Item get(int index){
		return this.items.get(index);
	}
	
	public void changeSize(int newSize){
		ArrayList<Item> newInventory = new ArrayList<Item>(newSize);
		int lenghtToUse = 0;
		if(this.items.size()<newSize)
			lenghtToUse = this.items.size();
		else
			lenghtToUse = newSize;
		for(int i=0;i<lenghtToUse;i++)
		{
			newInventory.add(this.items.get(i));
		}
		this.items = newInventory;
	}
}