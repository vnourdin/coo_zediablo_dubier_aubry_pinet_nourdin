package ia;

import gameEngine.Command;

public class Action {

	public Command command;
	public int orientation;
	
	public Action(Command command, int orientation){
		this.command = command;
		this.orientation = orientation;
	}
}
