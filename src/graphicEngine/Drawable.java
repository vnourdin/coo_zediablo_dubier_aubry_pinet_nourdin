package graphicEngine;

import java.awt.Graphics2D;

/**
 * Interface representant la maniere de dessiner sur un JPanel
 */
public interface Drawable {

	/**
	 * Methode qui affiche un element Dessinable sur l'image passee en parametre
	 * 
	 * @param graphics
	 *            image sur laquelle dessiner
	 */
	public abstract void draw(Graphics2D graphics);
}
