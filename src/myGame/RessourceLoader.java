package myGame;

import graphicEngine.AnimatedSprite;
import graphicEngine.ItemSprite;
import graphicEngine.CharacterSprite;
import gui.MyGameDraw;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import javax.imageio.ImageIO;

/*
 * Classe utilitaire utilisee pour charger les differentes ressources
 * statiques du jeu telles que les images sources, etc
 */
public class RessourceLoader {

	public static final int HEROmageWOMAN=0, HEROwarrior=1, HEROthief=2, HEROmageMAN=3;
	public static final int ENEMYO=0, ENEMYarcherBOW=1, ENEMYarcherKNIFE=2, ENEMYM=3;
	public static final int SPEAR_TRAP=0;

	/* Sprite de tous les objets */
	private static BufferedImage all_items;
	public static BufferedImage potionHeal;
	public static BufferedImage potionSpeed;
	public static BufferedImage potionRegen;
	public static BufferedImage potionEmpty;

	/* Sprites des heros */
	public static BufferedImage heromw_spritesheet;
	public static BufferedImage herow_spritesheet;
	public static BufferedImage heromm_spritesheet;
	public static BufferedImage herot_spritesheet;

	/* Sprites des ennemis */
	public static BufferedImage ennemio_spritesheet;
	public static BufferedImage ennemia_spritesheet;
	public static BufferedImage ennemia2_spritesheet;
	public static BufferedImage ennemim_spritesheet;

	/* Sprite des pieges */
	public static BufferedImage spearTrap;

	/* Sprites et Patterns des differents elements du jeu et de l'interface */
	public static BufferedImage invisible;
	public static BufferedImage arrow;
	public static BufferedImage ground;
	public static BufferedImage wall;
	public static BufferedImage stairs;
	public static BufferedImage heart;
	public static BufferedImage emptyHeart;
	
	/* Backgrounds */
	public static BufferedImage HUDBackground;
	public static BufferedImage gameLaunchBackground;
	public static BufferedImage bootBackground;

	/* Bouttons */
	public static BufferedImage playButton;
	public static BufferedImage playButtonRollover;
	public static BufferedImage playButtonPressed;
	
	public static BufferedImage gameButton;
	public static BufferedImage gameButtonRollover;
	public static BufferedImage gameButtonPressed;
	
	public static BufferedImage editorButton;
	public static BufferedImage editorButtonRollover;
	public static BufferedImage editorButtonPressed;
	
	/* Images associees aux personnages */
	public static BufferedImage heromw_Image;
	public static BufferedImage herow_Image;
	public static BufferedImage heromm_Image;
	public static BufferedImage herot_Image;

	/* Sprite des explosions */
	public static BufferedImage explosion;
	public static BufferedImage explosionMage;
	public static BufferedImage explosionMageEnemy;
	
	/* Musique */
	public static AudioClip music;

	public static void loadAll(){
		try{
			loadHeroesSprites();
			loadEnnemiesSprites();
			loadItemsSprites();

			loadBackgrounds();
			loadPlayerImages();
			
			loadButtons();
			loadMusic();
			
			loadOtherThings();
			loadRandomPatterns();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void loadHeroesSprites() throws IOException
	{
		heromw_spritesheet = ImageIO.read(new File("Ressources/Sprites/heroW_mage.png"));
		herot_spritesheet = ImageIO.read(new File("Ressources/Sprites/heroM_thief.png"));
		herow_spritesheet = ImageIO.read(new File("Ressources/Sprites/heroM_warrior.png"));
		heromm_spritesheet = ImageIO.read(new File("Ressources/Sprites/heroM_mage.png"));
	}

	private static void loadEnnemiesSprites() throws IOException
	{
		ennemio_spritesheet = ImageIO.read(new File("Ressources/Sprites/enemy_orc.png"));
		ennemia_spritesheet = ImageIO.read(new File("Ressources/Sprites/enemy_archer.png"));
		ennemia2_spritesheet = ImageIO.read(new File("Ressources/Sprites/enemy_archer2.png"));
		ennemim_spritesheet = ImageIO.read(new File("Ressources/Sprites/enemy_mage.png"));
	}
	
	private static void loadItemsSprites() throws IOException
	{
		all_items = ImageIO.read(new File("Ressources/Sprites/item_All.png")); 
		potionHeal = all_items.getSubimage(0*ItemSprite.ITEM_SIZE, 2*ItemSprite.ITEM_SIZE, ItemSprite.ITEM_SIZE, ItemSprite.ITEM_SIZE);
		potionSpeed = all_items.getSubimage(4*ItemSprite.ITEM_SIZE, 2*ItemSprite.ITEM_SIZE, ItemSprite.ITEM_SIZE, ItemSprite.ITEM_SIZE);
		potionRegen = all_items.getSubimage(5*ItemSprite.ITEM_SIZE, 2*ItemSprite.ITEM_SIZE, ItemSprite.ITEM_SIZE, ItemSprite.ITEM_SIZE);
		potionEmpty = all_items.getSubimage(2*ItemSprite.ITEM_SIZE, 4*ItemSprite.ITEM_SIZE, ItemSprite.ITEM_SIZE, ItemSprite.ITEM_SIZE);
	}

	private static void loadBackgrounds() throws IOException
	{
		HUDBackground = ImageIO.read(new File("Ressources/Backgrounds/HUDBackground.png"));
		gameLaunchBackground = ImageIO.read(new File("Ressources/Backgrounds/gameLaunchBackground.png"));
	}
	
	private static void loadPlayerImages() throws IOException
	{
		heromw_Image = reSize(ImageIO.read(new File("Ressources/Images/mageWoman.png")), 323, 400);
		herow_Image = reSize(ImageIO.read(new File("Ressources/Images/warrior.png")), 323, 400);
		heromm_Image = reSize(ImageIO.read(new File("Ressources/Images/mageMan.png")), 323, 400);
		herot_Image = reSize(ImageIO.read(new File("Ressources/Images/thief.png")), 323, 400);
	}
	
	private static void loadButtons() throws IOException
	{
		gameButton = ImageIO.read(new File("Ressources/Buttons/gameButton.png"));
		gameButtonRollover = ImageIO.read(new File("Ressources/Buttons/gameButtonRollover.png"));
		gameButtonPressed = ImageIO.read(new File("Ressources/Buttons/gameButtonPressed.png"));
		
		editorButton = ImageIO.read(new File("Ressources/Buttons/editorButton.png"));
		editorButtonRollover = ImageIO.read(new File("Ressources/Buttons/editorButtonRollover.png"));
		editorButtonPressed = ImageIO.read(new File("Ressources/Buttons/editorButtonPressed.png"));

		playButton = ImageIO.read(new File("Ressources/Buttons/playButton.png"));
		playButtonRollover = ImageIO.read(new File("Ressources/Buttons/playButtonRollover.png"));
		playButtonPressed = ImageIO.read(new File("Ressources/Buttons/playButtonPressed.png"));
	}
	
	public static void launchMusic()
	{
		try {
			loadMusic();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		music.loop();		
	}

	private static void loadMusic() throws MalformedURLException
	{
		int choice = (int) (Math.round(Math.random()*3)+1);
		music = Applet.newAudioClip(new File("Ressources/Sounds/sound"+choice+".aiff").toURI().toURL());
	}

	private static void loadOtherThings() throws IOException
	{
		spearTrap = ImageIO.read(new File("Ressources/Sprites/trap_SpearTrap.png"));
		arrow =  reSize(ImageIO.read(new File("Ressources/Sprites/arrow.png")),MyGameDraw.TILE_SIZE,MyGameDraw.TILE_SIZE);
		heart = reSize(ImageIO.read(new File("Ressources/Sprites/gui_Heart.png")), 25, 25);
		emptyHeart = reSize(ImageIO.read(new File("Ressources/Sprites/gui_EmptyHeart.png")), 25, 25);
		invisible = new BufferedImage(1,1,BufferedImage.TYPE_4BYTE_ABGR);
		Graphics2D g = invisible.createGraphics();
		g.setComposite(AlphaComposite.Clear);
		g.fillRect(0,0,1,1);
		g.dispose();

		explosion = ImageIO.read(new File("Ressources/Sprites/explosion.png"));
		explosionMage = ImageIO.read(new File("Ressources/Sprites/explosionMage.png"));
		explosionMageEnemy = ImageIO.read(new File("Ressources/Sprites/explosionMageEnemy.png"));
	}

	public static void loadRandomPatterns() throws IOException
	{
		int randomPatternChoice = (int) (Math.round(Math.random()*5)+1);
		wall = reSize(ImageIO.read(new File("Ressources/Patterns/wall"+randomPatternChoice+".png")),MyGameDraw.TILE_SIZE,MyGameDraw.TILE_SIZE);
		randomPatternChoice = (int) (Math.round(Math.random()*5)+1);
		ground = reSize(ImageIO.read(new File("Ressources/Patterns/ground"+randomPatternChoice+".png")),MyGameDraw.TILE_SIZE,MyGameDraw.TILE_SIZE);

		stairs = new BufferedImage(MyGameDraw.TILE_SIZE,MyGameDraw.TILE_SIZE,BufferedImage.TYPE_4BYTE_ABGR);
		Graphics2D dessin = stairs.createGraphics();
		dessin.drawImage(ground, 0, 0, null);
		BufferedImage temp = reSize(ImageIO.read(new File("Ressources/Patterns/stairs.png")),MyGameDraw.TILE_SIZE-10,MyGameDraw.TILE_SIZE);
		dessin.drawImage(temp, 0, 0, null);
		dessin.dispose();
	}

	public static BufferedImage reSize(BufferedImage source,int newWidth,int newHeight){
		double scaleW = (double)newWidth/source.getWidth();
		double scaleH = (double)newHeight/source.getHeight();
		BufferedImage reSizedImage = new BufferedImage(newWidth,newHeight,BufferedImage.TYPE_4BYTE_ABGR);
		Graphics2D draw = reSizedImage.createGraphics();
		draw.scale(scaleW,scaleH);
		draw.drawImage(source,0,0,null);
		draw.dispose();
		return reSizedImage;
	}

	//							//
	//	Heroes sprites getters	//
	//							//

	public static CharacterSprite getMageWomanSprite()
	{
		return new CharacterSprite(
				heromw_spritesheet,
				MyGameDraw.TILE_SIZE,
				MyGameDraw.TILE_SIZE
				);
	}

	public static CharacterSprite getWarriorSprite()
	{
		return new CharacterSprite(
				herow_spritesheet,
				MyGameDraw.TILE_SIZE,
				MyGameDraw.TILE_SIZE
				);
	}

	public static CharacterSprite getMageManSprite()
	{
		return new CharacterSprite(
				heromm_spritesheet,
				MyGameDraw.TILE_SIZE,
				MyGameDraw.TILE_SIZE
				);
	}

	public static CharacterSprite getThiefSprite()
	{
		return new CharacterSprite(
				herot_spritesheet,
				MyGameDraw.TILE_SIZE,
				MyGameDraw.TILE_SIZE
				);
	}

	//								//
	//	Ennemies sprites getters	//
	//								//

	public static CharacterSprite getOrcSprite()
	{
		return new CharacterSprite(
				ennemio_spritesheet,
				MyGameDraw.TILE_SIZE,
				MyGameDraw.TILE_SIZE
				);
	}

	public static CharacterSprite getArcherBowSprite()
	{
		return new CharacterSprite(
				ennemia_spritesheet,
				MyGameDraw.TILE_SIZE,
				MyGameDraw.TILE_SIZE
				);
	}

	public static CharacterSprite getArcherKnifeSprite()
	{
		return new CharacterSprite(
				ennemia2_spritesheet,
				MyGameDraw.TILE_SIZE,
				MyGameDraw.TILE_SIZE
				);
	}

	public static CharacterSprite getEnemyMageSprite()
	{
		return new CharacterSprite(
				ennemim_spritesheet,
				MyGameDraw.TILE_SIZE,
				MyGameDraw.TILE_SIZE
				);
	}

	//								//
	//	Explosions sprites getters	//
	//								//

	public static AnimatedSprite getExplosionSprite(){
		return new AnimatedSprite(
				explosion,
				25,
				0,0,
				5,5
				);
	}

	public static AnimatedSprite getExplosionMageSprite(){
		return new AnimatedSprite(
				explosionMage,
				25,
				0,0,
				5,5
				);
	}

	public static AnimatedSprite getExplosionMageEnemySprite(){
		return new AnimatedSprite(
				explosionMageEnemy,
				25,
				0,0,
				5,5
				);
	}

	//							//
	//	Traps sprites getters	//
	//							//

	public static AnimatedSprite getSpearTrapSprite(){
		AnimatedSprite trapSprite = new AnimatedSprite(
				spearTrap,
				12,
				0,0,
				4,3
				);
		trapSprite.setTimeWait(25);
		return trapSprite;
	}

	//							//
	//	Items sprites getters	//
	//							//

	public static ItemSprite getPotionToHealSprite(){
		return new ItemSprite(potionHeal);
	}

	public static ItemSprite getPotionToIncreaseSpeedSprite(){
		return new ItemSprite(potionSpeed);
	}

	public static ItemSprite getPotionToRegenerateSprite(){
		return new ItemSprite(potionRegen);
	}

}