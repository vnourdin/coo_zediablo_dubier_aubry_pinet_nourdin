package timer;

import entity.Character;

public class SpeedReset extends CharacterTimer {

	private int initialSpeed;
	
	public SpeedReset(long sleep, int initialSpeed, Character charact) {
		super(sleep,charact);
		this.initialSpeed = initialSpeed;
	}

	@Override
	public void tick() {
		this.charact.setMovingSpeed(initialSpeed);
	}
}