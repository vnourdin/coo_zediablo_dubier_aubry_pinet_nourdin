package factory;

import java.io.IOException;

import entity.Player;

public abstract class LevelFactory{

	protected Level level;
	
	protected LabyrinthFactory labyrinth_factory;
	protected MonstersFactory monsters_factory;
	protected ItemsFactory item_factory;
	
	public LevelFactory(Player player){
		this.level = new Level(player);
	}
	
	public Level generateLevel() throws IOException{
		this.prepareFactory();
		this.level.labyrinth = this.labyrinth_factory.generateLabyrinth();
		this.level.monsters  = this.monsters_factory.generateMonsters(this.level.labyrinth);
		this.level.items     = this.item_factory.generateItems(this.level.labyrinth);
		this.level.player.changeLabyrinth(this.level.labyrinth);
		this.level.player.setPosition(7,7);
		return level;
	}
	
	public abstract void prepareFactory();
}
