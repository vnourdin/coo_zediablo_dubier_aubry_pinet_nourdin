package entity;

import myGame.Main;
import environment.Labyrinth;
import graphicEngine.CharacterSprite;

public class Player extends Character{

	private String name;
	private long lifeRegen;
	private int regenSpeed;
	private int level;

	public Player(CharacterSprite sprite ,Labyrinth laby)
	{
		super(sprite,laby);
		this.name = "john";
		this.setLife(100);
		this.regenSpeed = 3000;
		this.level = 0;
	}

	public Player(CharacterSprite sprite, String name, Labyrinth lab)
	{
		super(sprite,lab);
		this.name = name;
		this.setLife(100);
		this.regenSpeed = 3000;
		this.level = 0;
	}

	@Override
	public void update(long delta_time){
		super.update(delta_time);
		this.regenerate(delta_time);
	}
	
	@Override
	public void die(){
		Main.gameEngine.setUpdateTime(125);
		super.die();
	}

	private void regenerate(long delta_time)
	{
		if(!this.isDeath()){
			this.lifeRegen += delta_time;
			if(this.lifeRegen > this.regenSpeed){
				this.cure(10);
				this.stopRegeneration();
			}
		}
	}

	@Override
	public void sufferDamages(int d){
		super.sufferDamages(d);
		this.stopRegeneration();
	}

	private void stopRegeneration(){ this.lifeRegen = 0; }	

	public void changeName(String newName){
		if(!newName.isEmpty())
			this.name = newName;
	}

	public void setRegen(int regen_speed){
		if(regen_speed>0)
			this.regenSpeed = regen_speed;
	}


	public void changeLabyrinth(Labyrinth labyrinth){
		this.labyrinth = labyrinth;
		this.labyrinth.addCharacter(this);
	}
	
	public void levelUp(){ this.level++; }
	
	public int getLevel(){ return this.level; }

	public String getName(){ return this.name; }

	public int getRegen(){ return this.regenSpeed; }

	@Override
	public String toString()
	{
		if(!this.isDeath())
			return ("Je m'appelle "+this.name+" et j'ai "+this.life+" points de vie. Je me trouve actuelement au point ("+this.x+","+this.y+").");
		else
			return ("Je m'appelle "+this.name+" et je suis mort..");
	}

	@Override
	public String XMLEntityName() {
		return "Player";
	}
}
