package behaviours;

import entity.Character;
import graphicEngine.CharacterSprite;

public abstract class Fight {

	protected CharacterSprite sprite;
	
	public Fight(CharacterSprite sprite){
		this.sprite = sprite;
	}
	
	public void attack(Character caract, int orientation, int damages){
		caract.sufferDamages(damages);
	}
}