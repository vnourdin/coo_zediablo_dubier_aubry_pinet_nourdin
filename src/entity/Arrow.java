package entity;


import environment.Labyrinth;
import gameEngine.Updateable;
import graphicEngine.Drawable;
import graphicEngine.CharacterSprite;
import graphicEngine.Sprite;
import gui.MyGameDraw;
import myGame.Main;
import myGame.RessourceLoader;

public class Arrow extends Entity implements Updateable{
	
	private Character source;
	private Labyrinth environment;
	private int orientation;

	public Arrow(Character source){
		super(new Sprite(RessourceLoader.arrow));
		this.setPosition(source.x,source.y);
		this.environment = source.getLabyrinth();
		this.source = source;
		this.orientation = source.getOrientation();
		switch(orientation){
		case CharacterSprite.DOWN:
			this.sprite.Rotate(Math.toRadians(180));
			break;
		case CharacterSprite.LEFT:
			this.sprite.Rotate(Math.toRadians(-90));
			break;
		case CharacterSprite.RIGHT:
			this.sprite.Rotate(Math.toRadians(90));
			break;
		}
	}

	@Override
	public void update(long delta_time) {
		if(!sprite.isMoving()){
			boolean move = false;
			switch(orientation){
			case CharacterSprite.UP:
				move = canMove(x,y-1);
				if(move)
					this.y--;
				break;
			case CharacterSprite.DOWN:
				move = canMove(x,y+1);
				if(move)
					this.y++;
				break;
			case CharacterSprite.LEFT:
				move = canMove(x-1,y);
				if(move)
					this.x--;
				break;
			case CharacterSprite.RIGHT:
				move = canMove(x+1,y);
				if(move)
					this.x++;
				break;
			}
			if(move){
				sprite.startTranslation(
						x*MyGameDraw.TILE_SIZE+MyGameDraw.TILE_SIZE/2,
						y*MyGameDraw.TILE_SIZE+MyGameDraw.TILE_SIZE/2,
						4
						);
			}
		}
		sprite.update(delta_time);
	}
	
	public boolean canMove(int x,int y){
		if(this.environment.isSolid(y,x)){
			Character touche = this.environment.getCharacterAt(y,x);
			if(touche!=null && touche!=source)
				touche.sufferDamages(10);
			Main.game.waitForDelete((Drawable)this);
			Main.game.waitForDelete((Updateable)this);
			return false;
		}else{
			return true;
		}
	}

	@Override
	public String XMLEntityName() {
		return "Arrow";
	}
}
