package editor;

import environment.Labyrinth;

public abstract class LabyrinthExporter {

	protected Labyrinth labyrinth;

	public LabyrinthExporter(Labyrinth labyrinth){
		this.labyrinth = labyrinth;
	}

	public abstract void exporter(String path) throws Exception;
}