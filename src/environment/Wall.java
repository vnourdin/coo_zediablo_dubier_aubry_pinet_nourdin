package environment;

import graphicEngine.Sprite;
import myGame.RessourceLoader;

public class Wall extends Tile{

	public Wall(int posX,int posY){
		super(new Sprite(RessourceLoader.wall),posX,posY,true);
		this.id = Tile.WALL;
	}
}