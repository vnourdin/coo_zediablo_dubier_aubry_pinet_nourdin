package gameEngine;

/**
 * Classe permettant de representer une commande de l'utilisateur
 */
public class Command {

	public boolean left, right, up, down;
	public boolean attack, pause;
	public int item;
	public boolean labitbucket, mynameistomtombinary;

	public Command()
	{
		this.item = -1;
	}
	
	/**
	 * constructeur par copie
	 * copie la commande pour en creer une nouvelle
	 * @param commandToCopy
	 */
	public Command(Command commandToCopy)
	{
		this.item = commandToCopy.item;
		this.down=commandToCopy.down;
		this.up=commandToCopy.up;
		this.left=commandToCopy.left;
		this.right=commandToCopy.right;
		this.attack = commandToCopy.attack;
		this.labitbucket = commandToCopy.labitbucket;
		this.mynameistomtombinary = commandToCopy.mynameistomtombinary;
	}
	
	public void randomMoving(){
		switch((int)(Math.random()*4)){
			case 0:
				this.up = true;
			break;
			case 1:
				this.down = true;
			break;
			case 2:
				this.left = true;
			break;
			case 3:
				this.right = true;
			break;
		}
	}
}