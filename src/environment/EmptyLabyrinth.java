package environment;

public class EmptyLabyrinth extends Labyrinth{

	public EmptyLabyrinth(int height, int width)
	{
		this.name = "Labyrinthe vide";
		this.column = width;
		this.line = height;
		this.contents = new Tile[this.line][this.column];
		for(int i=0; i<this.line; i++){
			for(int j=0; j<this.column; j++){
				this.contents[i][j] = new Tile(j,i);
			}
		}
		this.initialiseLists();
		this.setChanged();
	}
}