package editor;

import ia.IAPathFinding;
import ia.IARandom;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import entity.Monster;
import graphicEngine.CharacterSprite;

public class MonsterDialog extends JDialog implements ItemListener,ActionListener{

	private boolean valider = false;

	private JComboBox orientationChoice;
	private ImageIcon icon;
	private JCheckBox pathFindingChoice;
	private JButton okButton;
	private Monster monsterInConstruction;

	public MonsterDialog(Monster construct){
		super((Dialog)null,"Monster dialog",true);

		this.monsterInConstruction = construct;

		icon = new ImageIcon();
		JLabel preview = new JLabel(icon);
		orientationChoice = new JComboBox(new String[]{"HAUT","BAS","GAUCHE","DROITE"});
		pathFindingChoice = new JCheckBox("PathFinding");
		okButton = new JButton("Valider");

		orientationChoice.addItemListener(this);
		pathFindingChoice.addActionListener(this);
		okButton.addActionListener(this);

		this.monsterInConstruction.orientate(CharacterSprite.UP);
		this.monsterInConstruction.setIA(new IARandom(null));

		Image image = construct.getOverview();
		icon.setImage(image);
		preview.setPreferredSize(new Dimension(image.getWidth(null),image.getHeight(null)));

		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(150,150));

		panel.add(preview);
		panel.add(orientationChoice);
		panel.add(pathFindingChoice);
		panel.add(okButton);
		this.setContentPane(panel);
		this.pack();
	}

	public boolean showDialog(){
		this.setVisible(true);
		return valider;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource()==okButton){
			this.valider = true;
			this.setVisible(false);
		}else if(arg0.getSource()==pathFindingChoice){
			if(this.pathFindingChoice.isSelected()){
				this.monsterInConstruction.setIA(new IAPathFinding(null));
			}else{
				this.monsterInConstruction.setIA(new IARandom(null));
			}
		}
	}

	@Override
	public void itemStateChanged(ItemEvent arg0) {
		if(arg0.getSource()==this.orientationChoice){
			String s = (String)this.orientationChoice.getSelectedItem();
			if(s.equals("HAUT")){
				this.monsterInConstruction.orientate(CharacterSprite.UP);
			}else if(s.equals("BAS")){
				this.monsterInConstruction.orientate(CharacterSprite.DOWN);
			}else if(s.equals("GAUCHE")){
				this.monsterInConstruction.orientate(CharacterSprite.LEFT);
			}else if(s.equals("DROITE")){
				this.monsterInConstruction.orientate(CharacterSprite.RIGHT);
			}
			this.icon.setImage(this.monsterInConstruction.getOverview());
			this.repaint();
		}
	}
}