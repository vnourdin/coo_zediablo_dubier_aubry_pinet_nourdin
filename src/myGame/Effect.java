package myGame;


import entity.Entity;
import gameEngine.Updateable;
import graphicEngine.AnimatedSprite;
import graphicEngine.Drawable;

/**
 * Classe qui est utilisee pour les explosions
 */
public class Effect extends Entity implements Updateable {
	
	public Effect(AnimatedSprite sprite,int posX,int posY){
		super(sprite);
		setPosition(posX,posY);
		((AnimatedSprite)this.sprite).start();
	}
	
	@Override
	public void update(long delta_time) {
		this.sprite.update(delta_time);
		if(!((AnimatedSprite) this.sprite).isStarted()){
			Main.game.waitForDelete((Drawable)this);
			Main.game.waitForDelete((Updateable)this);
		}
	}

	@Override
	public String XMLEntityName() {
		return "Effect";
	}
}
