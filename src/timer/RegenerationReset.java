package timer;

import entity.Player;
import entity.Character;

public class RegenerationReset extends CharacterTimer{

	private int initialRegen;
	
	public RegenerationReset(long sleep, int initialRegen, Character charact) {
		super(sleep, charact);
		this.initialRegen = initialRegen;
	}

	@Override
	public void tick() {
		((Player)this.charact).setRegen(initialRegen);
	}
}
