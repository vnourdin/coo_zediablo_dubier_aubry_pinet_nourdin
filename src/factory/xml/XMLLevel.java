package factory.xml;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import entity.Player;
import factory.LevelFactory;

public class XMLLevel extends LevelFactory{

	
	private String path_xml_file;
	
	public XMLLevel(Player player,String path_xml_file) {
		super(player);
		this.path_xml_file = path_xml_file;
	}

	@Override
	public void prepareFactory(){
		File fXmlFile = new File(path_xml_file);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = null;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		Document doc = null;
		try {
			doc = dBuilder.parse(fXmlFile);
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}

		doc.getDocumentElement().normalize();
		Element labyrinth_xml =((Element)doc.getElementsByTagName("labyrinth").item(0));
		NodeList monsters_xml = doc.getElementsByTagName("monster");
		NodeList items_xml    = doc.getElementsByTagName("item");
		this.labyrinth_factory = new LabyrinthXML(labyrinth_xml);
		this.monsters_factory  = new MonstersXML(monsters_xml,level.player);
		this.item_factory      = new ItemsXML(items_xml);
	}

}
