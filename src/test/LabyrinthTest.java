package test;
import static org.junit.Assert.*;
import myGame.RessourceLoader;

import org.junit.Before;
import org.junit.Test;

import entity.OrcWarrior;
import entity.Monster;
import environment.EmptyLabyrinth;
import environment.Tile;
import environment.Labyrinth;

public class LabyrinthTest {

	@Before
	public void setUp(){
		try{
			RessourceLoader.loadAll();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Test
	public void isAddedTileReallyAdded(){
		Labyrinth laby = new EmptyLabyrinth(2,2);
		Tile t1 = new Tile(0,1);
		
		laby.setTile(t1);
		
		boolean isEmpty = !laby.isSolid(1,0);
		assertEquals("the tile (0,1) should be empty", true, isEmpty);
	}
	
	@Test
	public void areOutOfBoundsTilesSolids(){
		Labyrinth laby = new EmptyLabyrinth(2,2);
		
		boolean outofbound_up = laby.isSolid(-1,0);
		boolean outofbound_down = laby.isSolid(2,0);
		boolean outofbound_right = laby.isSolid(0,2);
		boolean outofbound_left = laby.isSolid(0,-1);
		
		assertEquals("Out of bounds up is solid", true, outofbound_up);
		assertEquals("Out of bounds down is solid", true, outofbound_down);
		assertEquals("Out of bounds right is solid", true, outofbound_right);
		assertEquals("Out of bounds left is solid", true, outofbound_left);
	}
	
	@Test
	public void isMonsterSolid(){
		Labyrinth laby = new EmptyLabyrinth(2,2);
		Monster m = new OrcWarrior(laby);
		laby.setTile(new Tile(0,0));
		m.setPosition(0,0);
		
		boolean isSolid = laby.isSolid(0,0);
		
		assertEquals("Should be solid", true, isSolid);
	}
	
	@Test
	public void isDeadMonsterSolid(){
		Labyrinth laby = new EmptyLabyrinth(2,2);
		Monster m = new OrcWarrior(laby);
		laby.setTile(new Tile(0,0));
		m.setPosition(0,0);
		
		m.die();
		boolean isSolid = laby.isSolid(0,0);
		
		assertEquals("Shouldn't be solid", false, isSolid);
	}
}
