package factory.random;

import ia.IA;
import ia.IAPathFinding;

import java.util.ArrayList;

import entity.ArcherSkeleton;
import entity.EnemyMage;
import entity.Monster;
import entity.OrcWarrior;
import entity.Player;
import environment.Labyrinth;
import factory.MonstersFactory;

public class MonstersRandom implements MonstersFactory {

	private Player target;
	
	public MonstersRandom(Player target){
		this.target = target;
	}
	
	private Monster createMonster(Labyrinth laby){
		int y = (int)(Math.random()*laby.getLine());
		int x = (int)(Math.random()*laby.getColumn());
		while(laby.isSolid(y,x)){
			y = (int)(Math.random()*laby.getLine());
			x = (int)(Math.random()*laby.getColumn());
		}
		Monster monster;
		int randomMonsterChoice = (int)(Math.random()*3);
		if(randomMonsterChoice==0){
			monster = new ArcherSkeleton(laby);
		}else if(randomMonsterChoice==1){
			monster = new OrcWarrior(laby);
		}else{
			monster = new EnemyMage(laby);
		}
		IA ia = new IAPathFinding(target);
		monster.setPosition(x, y);
		monster.setIA(ia);
		return monster;
	}
	
	@Override
	public ArrayList<Monster> generateMonsters(Labyrinth laby) {
		ArrayList<Monster> monsters = new ArrayList<Monster>();
		int randomNumberOfMonster = (int)(Math.random()*3)+4;
		for(int i=0; i<randomNumberOfMonster; i++)
			monsters.add(createMonster(laby));
		return monsters;
	}

}
