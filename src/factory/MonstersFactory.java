package factory;

import java.util.ArrayList;

import entity.Monster;
import environment.Labyrinth;

public interface MonstersFactory {
	public ArrayList<Monster> generateMonsters(Labyrinth labyrinth);
}
