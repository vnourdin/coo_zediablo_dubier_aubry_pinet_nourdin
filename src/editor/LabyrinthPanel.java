package editor;

import items.Item;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JPanel;

import entity.Character;
import entity.Entity;
import environment.Labyrinth;
import gui.MyGameDraw;

public class LabyrinthPanel extends JPanel implements Observer{

	private int current_tile_size = 42;

	private Labyrinth labyrinthInConstruction;
	private Entity entityToPlace;

	private double scaleFactor;
	private JFrame parent;

	public LabyrinthPanel(JFrame parent){
		this.parent = parent;
	}

	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		if(labyrinthInConstruction!=null)
		{
			Graphics2D g2d = (Graphics2D)g;

			g2d.scale(scaleFactor,scaleFactor);
			labyrinthInConstruction.draw(g2d);
			if(entityToPlace!=null) entityToPlace.draw(g2d);
			g2d.setColor(Color.BLACK);

			for(int i=0;i<this.labyrinthInConstruction.getLine()+1;i++)
				g2d.drawLine(0,i*MyGameDraw.TILE_SIZE,this.labyrinthInConstruction.getColumn()*MyGameDraw.TILE_SIZE,i*MyGameDraw.TILE_SIZE);

			for(int j=0;j<this.labyrinthInConstruction.getColumn()+1;j++)
				g2d.drawLine(j*MyGameDraw.TILE_SIZE,0,j*MyGameDraw.TILE_SIZE,this.labyrinthInConstruction.getLine()*MyGameDraw.TILE_SIZE);

			for(Character c : this.labyrinthInConstruction.getAllCharacters())
				c.draw(g2d);

			for(Item i : this.labyrinthInConstruction.getAllItems())
				i.draw(g2d);
		}
	}

	@Override
	public void update(Observable observable, Object arg1) {
		if(observable instanceof Labyrinth){
			this.labyrinthInConstruction = (Labyrinth)observable;
			this.setScaleFactorAndAdaptPreferredSize();
			this.parent.pack();
		}
		else if(observable instanceof Entity){
			this.entityToPlace = (Entity)observable;
		}
		this.requestFocus();
		this.revalidate();
		this.repaint();
	}

	public void zoomIn(){
		this.current_tile_size++;
		if(this.current_tile_size>100)
			this.current_tile_size = 100;
		this.setScaleFactorAndAdaptPreferredSize();
		this.revalidate();
		this.repaint();
	}

	public void zoomOut(){
		this.current_tile_size--;
		if(this.current_tile_size<20)
			this.current_tile_size=20;
		this.setScaleFactorAndAdaptPreferredSize();
		this.revalidate();
		this.repaint();
	}

	private void setScaleFactorAndAdaptPreferredSize()
	{
		this.scaleFactor = (double)current_tile_size/MyGameDraw.TILE_SIZE;
		this.setPreferredSize(new Dimension(
				this.labyrinthInConstruction.getColumn()*current_tile_size,
				this.labyrinthInConstruction.getLine()*current_tile_size)
				);
	}

	public int getCurrentTileSize(){
		return this.current_tile_size; 
	}
}