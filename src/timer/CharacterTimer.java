package timer;

import entity.Character;

public abstract class CharacterTimer extends TimerEffect{

	protected Character charact;
	
	public CharacterTimer(long sleep, Character charact){
		super(sleep);
		this.charact = charact;
		this.charact.addTimerEffect(this);
	}	
}