package editor;

import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class ToolsPanel extends JPanel{

	public ToolsPanel(EntityController controller,LabyrinthPanel labyPanel){
		JTabbedPane tabbedPane = new JTabbedPane();

		TilesPanel tilesPanel = new TilesPanel(controller,labyPanel);
		MonstersPanel monstersPanel = new MonstersPanel(controller,labyPanel);
		ItemsPanel itemsPanel = new ItemsPanel(controller,labyPanel);

		tabbedPane.addTab("Sols",tilesPanel);
		tabbedPane.addTab("Monstres",monstersPanel);
		tabbedPane.addTab("Objets",itemsPanel);
		tabbedPane.setPreferredSize(new Dimension(400,580));

		this.add(tabbedPane);
	}
}