package myGame;

import entity.Monster;
import entity.Player;
import environment.Labyrinth;
import factory.Level;
import factory.random.RandomLevelGenerator;
import gameEngine.Command;
import gameEngine.Game;
import gameEngine.Updateable;
import graphicEngine.Drawable;
import items.Item;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Classe qui contient les donnees du jeu.
 */
public class MyGame implements Game {

	private static final float end_scale = 2.5f;
	private float scale_effect = 1;
	private boolean startGame = false;
	/* le personnage du jeu */
	private Player player;
	private boolean over;
	/* le labyrinthe actuel */
	private Labyrinth laby;
	/* Liste d'attente des objets a creer ou supprimer */
	private ArrayList<Drawable> drawablesToRemove;
	private ArrayList<Updateable> updateablesToRemove;
	private ArrayList<Drawable> drawablesToAdd;
	private ArrayList<Updateable> updateablesToAdd;

	private ArrayList<Drawable> drawables;
	private ArrayList<Updateable> updateables;

	/**
	 * constructeur de jeu avec un Joueur et des Monstres, dans un labyrinthe aleatoire
	 */
	public MyGame() {
		this.drawables = new ArrayList<Drawable>();
		this.updateables = new ArrayList<Updateable>();
		this.drawablesToRemove = new ArrayList<Drawable>();
		this.updateablesToRemove = new ArrayList<Updateable>();
		this.drawablesToAdd = new ArrayList<Drawable>();
		this.updateablesToAdd = new ArrayList<Updateable>();
		this.over = false;
	}

	public void evolve(Command command,long delta_time) {
		cleanAndAddObject();
		updateScale();
		if(this.startGame){
			this.player.setAction(command);
			if(command.labitbucket)
				killAllMonsters();
			if(command.mynameistomtombinary){
				this.player.changeName("Tomtombinary");
				this.player.setLife(500);
				this.player.setInventoryCapacity(20);
				this.player.setInventorySize(20);
			}

			for(Updateable u : updateables){
				u.update(delta_time);
			}
			if(player.isTheLastSurvivor()){
				this.laby.activateAllStrairs();
			}
		}
	}

	private void updateScale(){
		if(scale_effect < end_scale){
			scale_effect+=0.025;
			if(scale_effect >= end_scale)
				startGame = true;
		}
	}

	public void loadRandomLevel() throws IOException{
		RessourceLoader.loadRandomPatterns();
		this.cleanDrawablesAndUpdateables();
		Level level = new RandomLevelGenerator(player).generateLevel();
		loadLevel(level);
	}

	public void loadLevel(Level level){
		this.scale_effect = 1;
		this.startGame = false;
		this.laby = level.labyrinth;
		this.drawablesToAdd.add(this.laby);
		this.updateablesToAdd.add(this.laby);

		for(Monster m : level.monsters){
			this.drawablesToAdd.add(m);
			this.updateablesToAdd.add(m);
		}

		for(Item i : level.items){
			this.drawablesToAdd.add(i);
			this.updateablesToAdd.add(i);
		}

		this.drawablesToAdd.add(player);
		this.updateablesToAdd.add(player);

		this.player.levelUp();
	}

	private void cleanDrawablesAndUpdateables()
	{
		for(Drawable dessinable : this.drawables)
			this.drawablesToRemove.add(dessinable);
		for(Updateable updateable : this.updateables)
			this.updateablesToRemove.add(updateable);
	}

	public void killAllMonsters(){
		for(int i=0;i<40;i++){
			int randX = (int)((Math.random()*16));
			int randY = (int)((Math.random()*16));
			generateExplosion(randX,randY);
		}

		for(Updateable u : updateables){
			if(u instanceof Monster)
				((Monster)u).die();
		}
	}

	public void generateExplosion(int x,int y){
		Effect explosion_effect = new Effect(
				RessourceLoader.getExplosionSprite(),
				x,
				y);
		waitForAdd((Updateable)explosion_effect);
		waitForAdd((Drawable)explosion_effect);
	}

	public void generateExplosionMage(int x,int y){
		Effect explosion_effect = new Effect(
				RessourceLoader.getExplosionMageSprite(),
				x,
				y);
		waitForAdd((Updateable)explosion_effect);
		waitForAdd((Drawable)explosion_effect);
	}

	public void generateExplosionMageEnnemi(int x,int y){
		Effect explosion_effect = new Effect(
				RessourceLoader.getExplosionMageEnemySprite(),
				x,
				y);
		waitForAdd((Updateable)explosion_effect);
		waitForAdd((Drawable)explosion_effect);
	}

	public void waitForDelete(Drawable object){
		this.drawablesToRemove.add(object);
	}

	public void waitForDelete(Updateable object){
		this.updateablesToRemove.add(object);
	}

	public void waitForAdd(Drawable object){
		this.drawablesToAdd.add(object);
	}

	public void waitForAdd(Updateable object){
		this.updateablesToAdd.add(object);
	}

	public void cleanAndAddObject(){
		for(Drawable object : drawablesToRemove)
			drawables.remove(object);
		drawablesToRemove.clear();

		for(Updateable object : updateablesToRemove)
			updateables.remove(object);
		updateablesToRemove.clear();

		for(Drawable object : drawablesToAdd)
			drawables.add(object);
		drawablesToAdd.clear();

		for(Updateable object : updateablesToAdd)
			updateables.add(object);
		updateablesToAdd.clear();
	}

	@Override
	public boolean isOver()
	{
		return this.over;
	}

	public ArrayList<Drawable> getDrawables()
	{
		return this.drawables;
	}

	public Labyrinth getLabyrinth()
	{
		return this.laby;
	}

	public Player getPlayer()
	{
		return (Player)this.player;
	}

	public void setPlayer(Player newPlayer) {
		this.player = newPlayer;
	}

	public String toString() {
		return ("" + this.player);
	}

	public float getScale(){
		return scale_effect;
	}
}
