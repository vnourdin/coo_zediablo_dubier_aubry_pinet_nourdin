package ia;

import java.awt.Color;
import java.awt.Graphics2D;

import entity.Monster;
import entity.Character;
import environment.Labyrinth;
import gameEngine.Command;
import graphicEngine.Drawable;
import graphicEngine.CharacterSprite;
import gui.MyGameDraw;

public class IAPathFinding extends IA implements Drawable{

	private Character target;
	
	private Labyrinth labyrinth;
	private int[][] abstractMap;
	
	public IAPathFinding(Character target){
		this.target = target;
	}
	
	@Override
	public void setSensor(Monster newSensor){
		super.setSensor(newSensor);
		this.labyrinth = this.sensor.getLabyrinth();
		if(this.labyrinth!=null){
			this.abstractMap = new int
					[this.labyrinth.getLine()]
							[this.labyrinth.getColumn()];
		}
	}
	
	public int findTheBestWay(int x,int y){
		for(int i=0;i<this.abstractMap.length;i++){
			for(int j=0;j<this.abstractMap[0].length;j++){
				this.abstractMap[i][j] = Integer.MAX_VALUE;
			}
		}
		this.abstractMap[y][x] = 0;
		this.spread(y,x);
		int value = this.abstractMap[sensor.y][sensor.x];

			if((sensor.y+1)<this.labyrinth.getLine()){
				if(this.abstractMap[sensor.y+1][sensor.x] < value){
					return CharacterSprite.DOWN;
				}
			}

			if((sensor.y-1)>=0){
				if(this.abstractMap[sensor.y-1][sensor.x] < value){
					return CharacterSprite.UP;
				}
			}

			if((sensor.x+1)<this.labyrinth.getColumn()){
				if(this.abstractMap[sensor.y][sensor.x+1] < value){
					return CharacterSprite.RIGHT;
				}
			}

			if((sensor.x-1)>=0){
				if(this.abstractMap[sensor.y][sensor.x-1] < value){
					return CharacterSprite.LEFT;
				}
			}
			return -1;
	}
	
	public void spread(int i,int j){
		int value = this.abstractMap[i][j];
		if((i+1)<this.labyrinth.getLine()){
			if(this.labyrinth.isSolid(i+1,j) && !(sensor.x==j && sensor.y==i+1))
				this.abstractMap[i+1][j] = Integer.MAX_VALUE;
			else if(value+1<this.abstractMap[i+1][j]){
				this.abstractMap[i+1][j] = value+1;
				this.spread(i+1,j);
			}
		}
		
		if((i-1)>=0){
			if(this.labyrinth.isSolid(i-1,j) && !(sensor.x==j && sensor.y==i-1))
				this.abstractMap[i-1][j] = Integer.MAX_VALUE;
			else if(value+1<this.abstractMap[i-1][j]){
				this.abstractMap[i-1][j] = value+1;
				this.spread(i-1,j);
			}
		}
		
		if((j-1)>=0){
			if(this.labyrinth.isSolid(i,j-1) && !(sensor.x==j-1 && sensor.y==i))
				this.abstractMap[i][j-1] = Integer.MAX_VALUE;
			else if(value+1<this.abstractMap[i][j-1]){
				this.abstractMap[i][j-1] = value+1;
				this.spread(i,j-1);
			}
		}
		
		if((j+1)<this.labyrinth.getColumn()){
			if(this.labyrinth.isSolid(i,j+1) && !(sensor.x==j+1 && sensor.y==i))
				this.abstractMap[i][j+1] = Integer.MAX_VALUE;
			else if(value+1<this.abstractMap[i][j+1]){
				this.abstractMap[i][j+1] = value+1;
				this.spread(i,j+1);
			}
		}
	}
	
	@Override
	public Action decide() {
		Command c = new Command();
		int orientation = findTheBestWay(this.target.x, this.target.y);
		switch(orientation){
		case CharacterSprite.UP:
			c.up = true;
			break;
		case CharacterSprite.DOWN:
			c.down = true;
			break;
		case CharacterSprite.LEFT:
			c.left = true;
			break;
		case CharacterSprite.RIGHT:
			c.right = true;
			break;
		}
		
		orientation = this.sensor.getOrientationFromCharacter(this.target);
		if(this.sensor.getTargetFromOrientation()==this.target && !target.isDeath())
			c.attack = true;
		return new Action(c,orientation);
	}

	@Override
	public void draw(Graphics2D g) {
		g.setColor(Color.BLACK);
		for(int i=0;i<this.abstractMap.length;i++){
			for(int j=0;j<this.abstractMap[0].length;j++){
				if(this.abstractMap[i][j]!=Integer.MAX_VALUE){
					g.drawString("["+this.abstractMap[i][j]+"]",j*MyGameDraw.TILE_SIZE+MyGameDraw.TILE_SIZE/2,i*MyGameDraw.TILE_SIZE+MyGameDraw.TILE_SIZE/2);
				}else{
					g.drawString("[X]",j*MyGameDraw.TILE_SIZE+MyGameDraw.TILE_SIZE/2,i*MyGameDraw.TILE_SIZE+MyGameDraw.TILE_SIZE/2);
				}
			}
		}
	}

	@Override
	public String xmlName() {
		return "PathFinding";
	}
}
