package environment;

import entity.Character;
import gameEngine.Updateable;
import graphicEngine.AnimatedSprite;
import graphicEngine.Sprite;
import gui.MyGameDraw;

import java.awt.Graphics2D;
import java.awt.Image;

import myGame.RessourceLoader;

public class Trap extends Tile implements Triggerable,Updateable{

	private AnimatedSprite trapAnimation;

	public Trap(int posX, int posY) {
		super(new Sprite(RessourceLoader.ground), posX, posY,false);
		this.trapAnimation = RessourceLoader.getSpearTrapSprite();
		this.trapAnimation.setPosition(
				this.x*MyGameDraw.TILE_SIZE+MyGameDraw.TILE_SIZE/2,
				this.y*MyGameDraw.TILE_SIZE+MyGameDraw.TILE_SIZE/2
				);
		this.id = Tile.TRAP;
	}

	@Override
	public void draw(Graphics2D graphics){
		super.draw(graphics);
		this.trapAnimation.draw(graphics);
	}

	@Override
	public void update(long delta_time) {
		this.trapAnimation.update(delta_time);
	}

	@Override
	public void triggerOnCharacter(Character charact) {
		if(!this.trapAnimation.isStarted() && charact.isOnTheSamePlace(this)){
			charact.sufferDamages(10);
			this.trapAnimation.start();
		}
	}

	@Override
	public Image getOverview(){
		return this.trapAnimation.getCurrent();
	}

	@Override
	public void setPosition(int x,int y){
		super.setPosition(x, y);
		if(this.trapAnimation!=null){
			this.trapAnimation.setPosition(
					this.x*MyGameDraw.TILE_SIZE+MyGameDraw.TILE_SIZE/2,
					this.y*MyGameDraw.TILE_SIZE+MyGameDraw.TILE_SIZE/2
					);
		}
	}
}