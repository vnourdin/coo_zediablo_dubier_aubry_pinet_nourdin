package factory.random;

import java.io.IOException;

import environment.Labyrinth;
import factory.LabyrinthFactory;
import factory.text.LabyrinthText;

public class LabyrinthTextRandom extends LabyrinthFactory{

	@Override
	public Labyrinth generateLabyrinth() throws IOException {
		int randomLevelChoice = (int) (Math.round(Math.random()*8)+1);
		String path = "Ressources/Levels/"+randomLevelChoice+".txt";
		LabyrinthText labyrinth_text = new LabyrinthText(path);
		this.labyrinthToBuild = labyrinth_text.generateLabyrinth();
		return labyrinthToBuild;
	}

}
