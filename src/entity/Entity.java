package entity;

import java.awt.Graphics2D;
import java.awt.Image;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import editor.Editable;
import graphicEngine.Drawable;
import graphicEngine.Sprite;
import gui.MyGameDraw;

public abstract class Entity extends Editable implements Drawable {

	protected boolean editingMode;
	protected Sprite sprite;
	public int x;
	public int y;
	
	public Entity(Sprite sprite){
		this.sprite = sprite;
		this.editingMode = false;
	}
	
	public void setPosition(int x, int y){
		this.x = x;
		this.y = y;
		this.sprite.setPosition(
				this.x*MyGameDraw.TILE_SIZE+MyGameDraw.TILE_SIZE/2,
				this.y*MyGameDraw.TILE_SIZE+MyGameDraw.TILE_SIZE/2
		);
		this.setChanged();
		this.notifyObservers();
	}
	
	public void draw(Graphics2D image) {
		this.sprite.draw(image);
	}
	
	public Sprite getSprite(){
		return this.sprite;
	}
	
	public void setSprite(Sprite sprite){
		this.sprite = sprite;
	}
	
	public Image getOverview(){
		return this.sprite.getCurrent();
	}
	
	public Element toXML(Document doc){
		Element item = doc.createElement(XMLEntityName());
		item.setAttribute("posX",Integer.toString(x));
		item.setAttribute("posY",Integer.toString(y));
		return item;
	}
	
	public abstract String XMLEntityName();
}
