package gui.launchGame;

import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class NameSelectionField extends JTextField {

	public NameSelectionField(ActionListener listener) {
		super();
		this.setColumns(15);
		this.addActionListener(listener);
		this.setDocument(new JTextFieldLimit(11));
	}

	private class JTextFieldLimit extends PlainDocument {
		private int limit;

		JTextFieldLimit(int limit) {
			super();
			this.limit = limit;
		}

		@Override
		public void insertString(int offset, String str, AttributeSet attr)
				throws BadLocationException {
			if (str == null)
				return;
			if ((getLength() + str.length()) <= limit) {
				super.insertString(offset, str, attr);
			}
		}
	}
}