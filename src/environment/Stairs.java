package environment;

import entity.Character;
import graphicEngine.Sprite;

import java.awt.Image;
import java.io.IOException;

import myGame.Main;
import myGame.RessourceLoader;

public class Stairs extends Tile implements Triggerable{

	private boolean activated;

	private Sprite switch_sprite;

	public Stairs(int x,int y){
		super(new Sprite(RessourceLoader.ground),x,y,false);
		this.id = Tile.STAIRS;
		this.activated = false;
		this.switch_sprite = new Sprite(RessourceLoader.stairs);
		this.switch_sprite.setPosition(sprite.getPosX(),sprite.getPosY());
	}

	@Override
	public void triggerOnCharacter(Character p) {
		if(p.isOnTheSamePlace(this) && activated)
			try {
				Main.game.loadRandomLevel();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}

	public void switchSprite(){
		Sprite temp = switch_sprite;
		this.switch_sprite = this.sprite;
		this.sprite = temp;
	}

	public void showStairs() {
		if(!activated){
			this.activated = true;
			switchSprite();
		}
	}

	public void hideStairs() {
		if(activated){
			this.activated = false;
			switchSprite();
		}
	}

	@Override
	public void startEditing(){
		showStairs();
		super.startEditing();
	}

	@Override
	public void stopEditing(){
		hideStairs();
		super.stopEditing();
	}

	@Override
	public Image getOverview(){
		return RessourceLoader.stairs;
	}
}