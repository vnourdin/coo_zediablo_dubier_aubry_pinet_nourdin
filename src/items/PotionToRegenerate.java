package items;

import timer.RegenerationReset;
import timer.TimerEffect;
import myGame.RessourceLoader;
import entity.Player;
import entity.Character;

public class PotionToRegenerate extends Item{

	public PotionToRegenerate() {
		super(RessourceLoader.getPotionToRegenerateSprite());
	}

	@Override
	public String getDescription() {
		return "Fixe la regeneration du joueur à 1 seconde pendant 5 seconde";
	}

	@Override
	public boolean use(Character user) {
		int last_regen = ((Player)user).getRegen();
		((Player)user).setRegen(500);
		TimerEffect timer = new RegenerationReset(5000,last_regen,(Player)user);
		timer.start();
		return true;
	}

	@Override
	public String getName() {
		return Item.POTION_REGENERATE;
	}
}