package factory.xml;

import items.Item;
import items.PotionToHeal;
import items.PotionToIncreaseSpeed;
import items.PotionToRegenerate;

import java.util.ArrayList;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import environment.Labyrinth;
import factory.ItemsFactory;

public class ItemsXML extends EntityFactoryXML<Item> implements ItemsFactory {
 
	private NodeList items_list;
	
	public ItemsXML(NodeList items_list){
		this.items_list = items_list;
	}
	
	@Override
	public ArrayList<Item> generateItems(Labyrinth labyrinth) {
		return this.createEntityListFromXML(labyrinth,items_list);
	}

	@Override
	public Item createEntityFromXML(Labyrinth labyrinth, Element entityXML) {
		String name = entityXML.getElementsByTagName("name").item(0).getTextContent();
		Item item = null;
		if(name.equals("PotionToHeal")){
			item = new PotionToHeal();
		}else if(name.equals("PotionToIncreaseSpeed")){
			item = new PotionToIncreaseSpeed();
		}else if(name.equals("PotionToRegenerate")){
			item = new PotionToRegenerate();
		}
		if(item!=null)
			labyrinth.addItem(item);
		return item;
	}

}
