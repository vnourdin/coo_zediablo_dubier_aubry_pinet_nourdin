package factory.xml;

import java.io.IOException;

import org.w3c.dom.Element;

import environment.Labyrinth;
import factory.LabyrinthFactory;
import factory.text.LabyrinthTextV2;

public class LabyrinthXML extends LabyrinthFactory{

	private Element labyrinth_xml;
	
	public LabyrinthXML(Element labyrinth_xml){
		this.labyrinth_xml = labyrinth_xml;
	}
	
	@Override
	public Labyrinth generateLabyrinth() throws IOException {
		int line = Integer.parseInt(this.labyrinth_xml.getAttribute("line"));
		int column = Integer.parseInt(this.labyrinth_xml.getAttribute("column"));
		String name = this.labyrinth_xml.getElementsByTagName("name").item(0).getTextContent();
		String datasourcetype =((Element)this.labyrinth_xml.getElementsByTagName("datasource").item(0)).getAttribute("type");
		String datasource = this.labyrinth_xml.getElementsByTagName("datasource").item(0).getTextContent();
		LabyrinthFactory factory = null;
		if(datasourcetype.equals("textfile")){
			factory = new LabyrinthTextV2(datasource,line,column); 
		}else
			System.err.println("Invalid datasourcetype : "+datasourcetype);
		this.labyrinthToBuild = factory.generateLabyrinth();
		this.labyrinthToBuild.setName(name);
		return this.labyrinthToBuild;
	}

}
