package behaviours;

import entity.Character;
import gameEngine.Command;

public abstract class Moving {

	protected Character pers;
	
	public Moving(Character pers){
		this.pers = pers;
	}
	
	public abstract void move(Command commande);
}
