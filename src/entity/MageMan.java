package entity;

import behaviours.FightWithExplosionSpell;
import myGame.RessourceLoader;
import environment.Labyrinth;
import graphicEngine.CharacterSprite;

public class MageMan extends Player{
	
	public MageMan(Labyrinth lab) {
		super(RessourceLoader.getMageManSprite(),lab);
		this.attackSpeed = 1000;
		this.range = 3;
		this.damages = 10;
		this.fight = new FightWithExplosionSpell((CharacterSprite) this.sprite);
	}
	
	public MageMan(String name, Labyrinth lab) {
		super(RessourceLoader.getMageManSprite(),name, lab);
		this.attackSpeed = 1000;
		this.range = 3;
		this.damages = 10;
		this.fight = new FightWithExplosionSpell((CharacterSprite) this.sprite);
	}
}