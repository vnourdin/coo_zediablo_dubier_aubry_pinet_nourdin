package graphicEngine;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

/**
 * JPanel du jeu utilise pour afficher le DessinMonJeu et le DessinInfo
 */
public class GamePannel extends JPanel{

	/**
	 * la classe chargee de Dessiner
	 */
	private Drawable drawing;

	private BufferedImage currentImage;
	private BufferedImage nextImage;
	private int imagesWidth, imagesHeight;

	/**
	 * constructeur qui construit les images pour doublebuffering ainsi que le
	 * Panel associe. Les images stockent le dessin et on demande au panel la
	 * mise a jour quand le dessin est fini
	 * 
	 * @param imagesWidth
	 *            largeur de l'image
	 * @param imagesHeight
	 *            hauteur de l'image
	 */
	public GamePannel(int width, int height, Drawable drawing) {
		super();
		this.setPreferredSize(new Dimension(width, height));
		this.imagesWidth = width;
		this.imagesHeight = height;
		this.drawing = drawing;

		this.nextImage = new BufferedImage(imagesWidth, imagesHeight, BufferedImage.TYPE_INT_ARGB);
		this.currentImage = new BufferedImage(imagesWidth, imagesHeight, BufferedImage.TYPE_INT_ARGB);
	}

	/**
	 * demande a mettre a jour le rendu de l'image sur le Panel. Cree une
	 * nouvelle image vide sur laquelle dessiner
	 */
	public void drawTheGame() {
		// generer la nouvelle image
		this.drawing.draw((Graphics2D)this.nextImage.getGraphics());

		// inverse les images doublebuffereing
		BufferedImage temp = this.currentImage;
		// l'image a dessiner est celle qu'on a construite
		this.currentImage = this.nextImage;
		// l'ancienne image est videe
		this.nextImage = temp;
		this.nextImage.getGraphics().dispose();
		// met a jour l'image a afficher sur le panel
		this.repaint();
	}

	public void paint(Graphics g) {
		super.paint(g);
		g.drawImage(this.currentImage, 0, 0, this.getWidth(), this.getHeight(), 0, 0, this.getWidth(), this.getHeight(), null);
	}
}