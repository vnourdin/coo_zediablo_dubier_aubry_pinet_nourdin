package graphicEngine;

import gameEngine.Updateable;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

/**
 * Classe utilisee pour dessiner des images
 */
public class Sprite implements Drawable,Updateable{
	/* Position du sprite en pixels */
	protected int posX,posY;
	/* Position d'arrive du sprite */
	protected int posX_end,posY_end;
	protected int frameCount = 0;
	protected int incX,incY;
	
	/* Echelle en x et en y */
	protected double scaleX,scaleY;
	
	/* Rotation a partir du centre en radians */
	protected double theta;
	
	/* Image source */
	protected BufferedImage source;
	/* Image a afficher */
	protected BufferedImage current;
	
	/**
	 * Construit un sprite
	 * l'image afficher sera l'image source
	 * @param source
	 *  image source
	 */
	public Sprite(BufferedImage source){
		this.source  = source;
		this.current = source;
		this.theta = 0;
		this.scaleX = 1;
		this.scaleY = 1;
		this.posX = 0;
		this.posY = 0;
	}
	
	/**
	 * Construit une sprite redimensionnee
	 * @param source
	 *  image source
	 * @param width
	 *  largeur de l'image a afficher
	 * @param height
	 *  hauteur de l'image a afficher
	 */
	public Sprite(BufferedImage source,int width,int height){
		this.source = source;
		this.theta = 0;
		this.scaleX = 1;
		this.scaleY = 1;
		this.posX = 0;
		this.posY = 0;
		this.ReSize(width,height);
	}
	
	/**
	 * Créer une nouvelle image redimensionne à partir de l'image source
	 * @param newWidth
	 * 	nouvelle largeur
	 * @param newHeight
	 *  nouvelle hauteur
	 */
	public void ReSize(int newWidth,int newHeight){
		double scaleW = (double)newWidth/this.source.getWidth();
		double scaleH = (double)newHeight/this.source.getHeight();
		this.current = new BufferedImage(newWidth,newHeight,BufferedImage.TYPE_4BYTE_ABGR);
		Graphics2D dessin = current.createGraphics();
		dessin.scale(scaleW,scaleH);
		dessin.drawImage(source,0,0,null);
		dessin.dispose();
	}
	
	/**
	 * Methode de rotation
	 * tourne le sprite
	 * @param angleRad
	 *  angle en radians
	 */
	public void Rotate(double angleRad){
		this.current = new BufferedImage(source.getWidth(),source.getHeight(),BufferedImage.TYPE_4BYTE_ABGR);
		Graphics2D dessin = current.createGraphics();
		dessin.rotate(angleRad,source.getWidth()/2,source.getHeight()/2);
		dessin.drawImage(source,0,0,null);
		dessin.dispose();
	}
	
	/**
	 * Defini l'echelle calcule pendant le rendu
	 * @param scaleX
	 * 	echelle sur l'axe X
	 * @param scaleY
	 * 	echelle sur l'axe Y
	 */
	public void scale(double scaleX,double scaleY){
		this.scaleX = scaleX;
		this.scaleY = scaleY;
	}
	
	/**
	 * Defini la position centrale de l'image en pixel
	 * @param posX
	 * 	position en pixel sur l'axe X
	 * @param posY
	 * 	position en pixel sur l'axe Y
	 */
	public void setPosition(int posX,int posY){
		this.posX = posX;
		this.posY = posY;
		this.frameCount = 0;
		this.incX = 0;
		this.incY = 0;
	}
	
	public void startTranslation(int posX,int posY,int nFrame){
		this.frameCount = nFrame;
		this.posX_end = posX;
		this.posY_end = posY;
		this.incX = (posX-this.posX)/nFrame;
		this.incY = (posY-this.posY)/nFrame;
	}
	
	public boolean isMoving(){
		return (this.frameCount!=0);
	}
	/**
	 * Defini la rotation de l'image calcule pendant le rendu
	 * @param theta
	 *  la rotation en radians
	 */
	public void setRotation(double theta){
		this.theta = theta;
	}
	
	@Override
	public void draw(Graphics2D g) {
		AffineTransform svg = g.getTransform(); //Sauvegarde la matrice de transformation
		g.translate(posX,posY);
		g.scale(scaleX,scaleY);
		g.rotate(theta);
		g.drawImage(current,-current.getWidth()/2,-current.getHeight()/2,null);
		g.setTransform(svg); //Restaure la matrice de transformation
	}

	public int getPosX() {
		return posX;
	}

	public int getPosY() {
		return posY;
	}

	public double getScaleX() {
		return scaleX;
	}

	public double getScaleY() {
		return scaleY;
	}

	public double getTheta() {
		return theta;
	}

	@Override
	public void update(long delta_time) {
		if(frameCount>0){
			this.posX+=incX;
			this.posY+=incY;
			this.frameCount--;
			if(frameCount==0){
				if(this.posX!=this.posX_end)
					this.posX = posX_end;
				if(this.posY!=this.posY_end)
					this.posY = posY_end;
			}
		
		}
	}
	
	public BufferedImage getCurrent(){
		return this.current;
	}

}
