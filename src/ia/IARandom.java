package ia;

import entity.Character;
import gameEngine.Command;

public class IARandom extends IA{

	private Character target;
	
	public IARandom(Character target) {
		this.target = target;
	}

	@Override
	public Action decide() {
		Action action = new Action(new Command(),0);
		action.orientation = sensor.getOrientationFromCharacter(target);
		if(this.sensor.getTargetFromOrientation()==target && !target.isDeath())
			action.command.attack = true;
		else
			action.command.randomMoving();
		return action;
	}

	@Override
	public String xmlName() {
		return "Random";
	}
}
