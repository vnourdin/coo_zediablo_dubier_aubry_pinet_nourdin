package gui.pause;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

import entity.Player;
import gameEngine.GameEngine;
import graphicEngine.Drawable;
import gui.GuiLife;
import myGame.MyGame;

public class StatsPanel extends JPanel implements Drawable {

	private GameEngine gameEngine;
	private Player player;
	private GuiLife playerGuiLife;

	public StatsPanel(GameEngine gameEngine) {
		this.setGameEngine(gameEngine);
		this.initialisePlayerAndGuiLife();
		this.setPreferredSize(
				new Dimension(this.gameEngine.getGUI().getWidth() / 2, this.gameEngine.getGUI().getHeight() / 5));
	}

	private void setGameEngine(GameEngine gameEngine) {
		this.gameEngine = gameEngine;
	}

	private void initialisePlayerAndGuiLife() {
		this.player = ((MyGame) (this.gameEngine.getGame())).getPlayer();
		this.playerGuiLife = new GuiLife(150, 30);
		this.playerGuiLife.setPlayer(this.player);
	}

	private void actualisePlayerAndGuiLife() {
		this.player = ((MyGame) (this.gameEngine.getGame())).getPlayer();
		this.playerGuiLife.setPlayer(this.player);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		this.actualisePlayerAndGuiLife();
		this.draw((Graphics2D) g);
	}

	@Override
	public void draw(Graphics2D g2d) {
		this.playerGuiLife.draw(g2d);
	}
}