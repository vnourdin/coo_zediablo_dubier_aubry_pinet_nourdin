package gui;

import graphicEngine.Drawable;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics2D;
import java.io.File;
import java.io.IOException;

import myGame.MyGame;
import myGame.RessourceLoader;

public class HUD implements Drawable{

	private MyGame game;
	private GuiText gtLevel, gtPlayer;
	private GuiTextToLine gtLaby;
	private GuiLife ghPlayer;
	private GuiInventory giPlayer;
	
	public HUD(MyGame game)
	{
		this.game = game;
		this.gtLaby = new GuiTextToLine(672, 50);
		this.gtPlayer = new GuiText(672, 150);
		this.gtLevel = new GuiText(672, 190);
		try {
			this.gtLaby.setFont(Font.createFont(Font.TRUETYPE_FONT, new File("Ressources/Fonts/Dungeon.ttf")),50, Font.BOLD);
		} catch (FontFormatException | IOException e) {
			System.out.println(e.getMessage());
		}
		this.ghPlayer = new GuiLife(672, 220);
		this.giPlayer = new GuiInventory(687, 250);
	}
	
	@Override
	public void draw(Graphics2D g2d) {
		g2d.drawImage(RessourceLoader.HUDBackground, 672, 0, 200, 672, null);
		this.gtLaby.setText(this.game.getLabyrinth().getName());
		this.gtLaby.draw(g2d);
		this.gtLevel.setText("Level : "+String.valueOf(this.game.getPlayer().getLevel()));
		this.gtLevel.draw(g2d);
		this.gtPlayer.setText(this.game.getPlayer().getName());
		this.gtPlayer.draw(g2d);
		this.ghPlayer.setPlayer(this.game.getPlayer());
		this.ghPlayer.draw(g2d);
		this.giPlayer.setPlayer(this.game.getPlayer());
		this.giPlayer.setHeight(this.ghPlayer.getHeightUsed());
		this.giPlayer.draw(g2d);
	}
}
