package editor;

import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

public class ZoomController implements MouseWheelListener{

	@Override
	public void mouseWheelMoved(MouseWheelEvent mwEvent) {
		LabyrinthPanel panel = (LabyrinthPanel)mwEvent.getSource();
		if(mwEvent.getWheelRotation()>0)
			panel.zoomIn();
		else
			panel.zoomOut();
	}
}