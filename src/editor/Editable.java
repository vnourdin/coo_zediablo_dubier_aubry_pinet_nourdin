package editor;

import java.util.Observable;

public abstract class Editable extends Observable{

	protected boolean editingMode;
	
	public void startEditing(){
		this.editingMode = true;
		this.setChanged();
		this.notifyObservers();
	}
	
	public void stopEditing(){
		this.editingMode = false;
		this.setChanged();
		this.notifyObservers();
	}
	
	public boolean isEditing(){
		return this.editingMode;
	}
}
