package gui.pause;

import gameEngine.GameEngine;

import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;


public class PausePanel extends JPanel implements KeyListener {

	private GameEngine gameEngine;
	private JPanel buttonsPanel, overViewPanel;

	public PausePanel(GameEngine gameEngine) {
		this.setGameEngine(gameEngine);
		this.createPanels();
		this.manageLayout();
		this.addPanels();
		this.gameEngine.getGUI().addKeyListener(this);
	}

	private void setGameEngine(GameEngine gameEngine) {
		this.gameEngine = gameEngine;
	}

	private void createPanels() {
		this.buttonsPanel = new ButtonsPanel(this.gameEngine);
		this.overViewPanel = new OverViewPanel(this.gameEngine);
	}

	private void manageLayout() {
		this.setLayout(new GridLayout(1, 2));
	}

	private void addPanels() {
		this.add(this.buttonsPanel);
		this.add(this.overViewPanel);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			if (this.gameEngine.isPaused())
				this.gameEngine.unpause();
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}
}
