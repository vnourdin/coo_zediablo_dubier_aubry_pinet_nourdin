package factory.xml;

import ia.IAPathFinding;
import ia.IARandom;

import java.util.ArrayList;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import entity.ArcherSkeleton;
import entity.EnemyMage;
import entity.Monster;
import entity.OrcWarrior;
import entity.Player;
import environment.Labyrinth;
import factory.MonstersFactory;

public class MonstersXML extends EntityFactoryXML<Monster> implements MonstersFactory {

	private NodeList monsters_list;
	private Player target;
	
	public MonstersXML(NodeList monsters_list,Player target){
		this.monsters_list = monsters_list;
		this.target = target;
	}
	
	@Override
	public ArrayList<Monster> generateMonsters(Labyrinth labyrinth) {
		return this.createEntityListFromXML(labyrinth,this.monsters_list);
	}

	@Override
	public Monster createEntityFromXML(Labyrinth labyrinth, Element entityXML) {
		String type = entityXML.getAttribute("type");
		Monster monster = null;
		if(type.equals("ArcherSkeleton"))
			monster = new ArcherSkeleton(labyrinth);
		else if(type.equals("EnemyMage"))
			monster = new EnemyMage(labyrinth);
		else if(type.equals("OrcWarrior"))
			monster = new OrcWarrior(labyrinth);
		
		String IA_type = entityXML.getElementsByTagName("IAConstant").item(0).getTextContent();
		if(IA_type.equals("PathFinding"))
			monster.setIA(new IAPathFinding(target));
		else
			monster.setIA(new IARandom(target));
		return monster;
	}

}
