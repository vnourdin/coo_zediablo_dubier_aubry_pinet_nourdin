package items;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import entity.Entity;
import entity.Character;
import gameEngine.Updateable;
import graphicEngine.ItemSprite;

public abstract class Item extends Entity implements Updateable{
	
	public static String POTION_HEAL="PotionToHeal",POTION_INCREASE_SPEED="PotionToIncreaseSpeed",POTION_REGENERATE="PotionToRegenerate";
	
	public Item(ItemSprite sprite) {
		super(sprite);
	}

	public void update(long delta_time) {
		this.sprite.update(delta_time);
	}
	
	@Override
	public String toString(){
		return this.getDescription();
	}
	
	@Override
	public Element toXML(Document doc){
		Element item = super.toXML(doc);
		Element name = doc.createElement("name");
		name.setTextContent(getName());
		item.appendChild(name);
		return item;
	}
	
	@Override
	public String XMLEntityName(){
		return "item";
	}
	
	public abstract String getDescription();
	public abstract String getName();
	public abstract boolean use(Character user);
}
