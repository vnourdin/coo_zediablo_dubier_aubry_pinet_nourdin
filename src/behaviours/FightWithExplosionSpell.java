package behaviours;

import entity.Character;
import graphicEngine.CharacterSprite;
import myGame.Main;

public class FightWithExplosionSpell extends Fight{

	public FightWithExplosionSpell(CharacterSprite sprite){
		super(sprite);
	}
	
	@Override
	public void attack(Character charact,int orientation,int damages){
		sprite.play(CharacterSprite.SPELL,orientation);
		Main.game.generateExplosionMage(charact.x,charact.y);
		super.attack(charact, orientation, damages);
	}
}
