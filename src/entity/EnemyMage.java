package entity;

import behaviours.FightWithEnemyExplosionSpell;
import environment.Labyrinth;
import graphicEngine.CharacterSprite;
import myGame.RessourceLoader;

public class EnemyMage extends Monster{

	public EnemyMage(Labyrinth laby){
		super(RessourceLoader.getEnemyMageSprite(), laby);
		this.setLife(20);
		this.range = 3;
		this.attackSpeed = 1300;
		this.fight = new FightWithEnemyExplosionSpell((CharacterSprite)this.sprite);
		this.damages = 15;
	}

	@Override
	public String monsterType() {
		return Monster.ENEMY_MAGE;
	}

	@Override
	public String xmlType() {
		return "EnemyMage";
	}
}
