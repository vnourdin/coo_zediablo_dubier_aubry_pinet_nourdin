package items;

import myGame.RessourceLoader;
import timer.SpeedReset;
import timer.TimerEffect;
import entity.Character;

public class PotionToIncreaseSpeed extends Item{

	public PotionToIncreaseSpeed() {
		super(RessourceLoader.getPotionToIncreaseSpeedSprite());
	}

	@Override
	public String getDescription() {
		return "Augmente la vitesse du joueur pendant 5 secondes";
	}

	@Override
	public boolean use(Character user) {
		int last_speed = user.getMovingSpeed();
		user.setMovingSpeed(7);
		TimerEffect timer = new SpeedReset(5000,last_speed,user);
		timer.start();
		return true;
	}

	@Override
	public String getName() {
		return Item.POTION_INCREASE_SPEED;
	}
}