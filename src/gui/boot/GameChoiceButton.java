package gui.boot;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import myGame.RessourceLoader;

public class GameChoiceButton extends JButton{
	
	public GameChoiceButton()
	{
		super(new ImageIcon(RessourceLoader.gameButton));
		this.setPressedIcon(new ImageIcon(RessourceLoader.gameButtonPressed));
		this.setRolloverIcon(new ImageIcon(RessourceLoader.gameButtonRollover));
		this.setContentAreaFilled(false);
		this.setBorderPainted(false);
		this.setFocusPainted(false);
	}
}
