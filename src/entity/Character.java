package entity;

import java.awt.Image;
import java.util.ArrayList;

import behaviours.Fight;
import behaviours.Moving;
import behaviours.Walking;
import timer.TimerEffect;
import items.Inventory;
import items.Item;
import environment.Labyrinth;
import gameEngine.Command;
import gameEngine.Updateable;
import graphicEngine.Drawable;
import graphicEngine.CharacterSprite;
import gui.MyGameDraw;
import myGame.Main;

/**
 * la classe correspondant aux Personnages
 */
public abstract class Character extends Entity implements Updateable {
	/* Environement */
	protected Labyrinth labyrinth;
	/* Comportements */
	protected Fight fight;
	protected Moving moving;

	protected int life;
	protected int damages;
	protected int maxLife;
	protected boolean death;

	protected int attackSpeed;
	protected int movingSpeed;
	protected int range;
	protected int orientation;
	protected int inventoryMaxSize;

	protected Inventory inventory;
	protected Command action;
	protected long attack_timer;

	/* Timer temporaire pour les effets des potions par exemple */
	protected ArrayList<TimerEffect> timerEffects;

	/**
	 * constructeur vide
	 */
	public Character(CharacterSprite sprite, Labyrinth labyrinthe) {
		super(sprite);
		this.labyrinth = labyrinthe;
		this.inventoryMaxSize = 10;
		this.inventory = new Inventory(this.inventoryMaxSize);
		if (labyrinthe != null) {
			this.labyrinth.addCharacter(this);
			this.setPosition(labyrinthe.getLine() / 2,
					labyrinthe.getColumn() / 2);
		}
		this.moving = new Walking(this);
		this.life = 100;
		this.attackSpeed = 750;
		this.range = 1;
		this.action = new Command();
		this.orientation = CharacterSprite.UP;
		this.movingSpeed = 14;
		this.timerEffects = new ArrayList<TimerEffect>();
	}

	@Override
	public void update(long delta_time) {
		if (this.actionIsPossible()) {
			Character target = this.getTargetFromOrientation();
			if (this.attackIsPossible(target))
				this.attack(target);
			else
				this.move();
		}
		if (this.itemEffectIsAvailable())
			this.useAnItem(action.item);

		this.updateAndCleanTimerEffect(delta_time);
		this.updateSprite(delta_time);
	}

	private boolean actionIsPossible() {
		return (!death && !sprite.isMoving());
	}

	private boolean attackIsPossible(Character target) {
		boolean result = false;
		if (action.attack && target != null) {
			if (this.attack_timer > this.attackSpeed) {
				result = true;
			}
		}
		return result;
	}

	public void attack(Character target) {
		this.fight.attack(target, orientation, damages);
		this.attack_timer = 0;
	}

	private void move() {
		this.moving.move(this.action);
		this.take(this.labyrinth.getItemAt(this.x, this.y));
	}

	public void take(Item item) {
		if (item != null) {
			if (inventory.add(item)) {
				this.labyrinth.removeItem(item);
				Main.game.waitForDelete((Drawable) item);
				Main.game.waitForDelete((Updateable) item);
			}
		}
	}

	private boolean itemEffectIsAvailable() {
		return (this.action.item != -1);
	}

	public void useAnItem(int index) {
		this.inventory.use(index, this);
	}

	public void sufferDamages(int d) {
		if (!this.isDeath()) {
			this.life -= d;
			if (this.life <= 0)
				this.die();
		}
	}

	public void cure(int s) {
		if (!this.isDeath()) {
			this.life += s;
			if (this.life > this.maxLife)
				this.life = this.maxLife;
		}
	}

	public void die() {
		this.life = 0;
		this.death = true;
		this.inventory.destroyInventory(x, y, this.labyrinth);
		((CharacterSprite) this.sprite).play(CharacterSprite.DIE, orientation);
	}

	private void updateSprite(long delta_time) {
		this.sprite.update(delta_time);
	}

	private void updateAndCleanTimerEffect(long delta_time) {
		ArrayList<TimerEffect> toRemove = new ArrayList<TimerEffect>();

		for (TimerEffect timerEffect : this.timerEffects) {
			timerEffect.update(delta_time);
			if (timerEffect.isStoped())
				toRemove.add(timerEffect);
		}

		for (TimerEffect timerEffect : toRemove)
			this.timerEffects.remove(timerEffect);

		this.attack_timer += delta_time;
	}

	public void setAction(Command c) {
		this.action = c;
	}

	public boolean isTheLastSurvivor() {
		if (!this.isDeath()) {
			for (int i = 0; i < this.labyrinth.getAllCharacters().size(); i++) {
				if (this != this.labyrinth.getAllCharacters().get(i)) {
					if (!this.labyrinth.getAllCharacters().get(i).isDeath()) {
						return false;
					}
				}
			}
			return true;
		} else
			return false;
	}

	public void orientate(int orientation) {
		this.orientation = orientation;
	}

	public Character getTargetFromOrientation() {
		switch (this.orientation) {
		case CharacterSprite.UP:
			for (int i = this.y; i >= (this.y - this.range); i--) {
				Character charact = this.labyrinth.getCharacterAt(i, this.x);
				if (charact != null && charact != this)
					return charact;
			}
			break;
		case CharacterSprite.DOWN:
			for (int i = this.y; i <= (this.y + this.range); i++) {
				Character charact = this.labyrinth.getCharacterAt(i, this.x);
				if (charact != null && charact != this)
					return charact;
			}
			break;
		case CharacterSprite.LEFT:
			for (int i = this.x; i >= (this.x - this.range); i--) {
				Character charact = this.labyrinth.getCharacterAt(this.y, i);
				if (charact != null && charact != this)
					return charact;
			}
			break;
		case CharacterSprite.RIGHT:
			for (int i = this.x; i <= (this.x + this.range); i++) {
				Character charact = this.labyrinth.getCharacterAt(this.y, i);
				if (charact != null && charact != this)
					return charact;
			}
			break;
		}
		return null;
	}

	public int getOrientationFromCharacter(Character target) {
		if (this.x > target.x)
			return CharacterSprite.LEFT;

		else if (this.x < target.x)
			return CharacterSprite.RIGHT;

		else if (this.y > target.y)
			return CharacterSprite.UP;

		else
			return CharacterSprite.DOWN;
	}

	public int getLife() {
		return this.life;
	}

	public void setLife(int life) {
		this.life = life;
		this.maxLife = life;
	}

	public boolean isDeath() {
		return this.death;
	}

	public CharacterSprite getCharacterSprite() {
		return (CharacterSprite) this.sprite;
	}

	public Labyrinth getLabyrinth() {
		return this.labyrinth;
	}

	public int getOrientation() {
		return this.orientation;
	}

	public void setSprite(CharacterSprite sprite) {
		this.sprite = sprite;
		this.sprite.setPosition(this.x * MyGameDraw.TILE_SIZE
				+ MyGameDraw.TILE_SIZE / 2, this.y * MyGameDraw.TILE_SIZE
				+ MyGameDraw.TILE_SIZE / 2);
	}

	public boolean isOnTheSamePlace(Entity entity) {
		return (entity.x == this.x && entity.y == this.y);
	}

	public void addTimerEffect(TimerEffect timer) {
		this.timerEffects.add(timer);
	}

	public void setMovingSpeed(int speed) {
		this.movingSpeed = speed;
	}

	public int getMovingSpeed() {
		return this.movingSpeed;
	}

	public String toString() {
		return ("(" + this.x + "," + this.y + ")");
	}

	public Inventory getInventory() {
		return this.inventory;
	}

	public int getInventoryMaxSize() {
		return this.inventoryMaxSize;
	}

	public void setInventoryCapacity(int newMaxSize) {
		this.inventoryMaxSize = newMaxSize;
	}

	public void setInventorySize(int newSize) {
		this.inventory.changeSize(newSize);
	}

	public int getMaxLife() {
		return this.maxLife;
	}

	@Override
	public Image getOverview() {
		((CharacterSprite) sprite).play(CharacterSprite.MOVING,
				this.orientation);
		sprite.update(0);
		return super.getOverview();
	}

}
