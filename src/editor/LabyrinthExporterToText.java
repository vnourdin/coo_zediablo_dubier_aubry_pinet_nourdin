package editor;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import environment.Labyrinth;

public class LabyrinthExporterToText extends LabyrinthExporter{

	public LabyrinthExporterToText(Labyrinth labyrinth) {
		super(labyrinth);
	}

	@Override
	public void exporter(String path) throws IOException {
		PrintWriter out = new PrintWriter(new FileWriter(path));
		out.println(labyrinth.toTxtV2());
		out.close();
	}
}