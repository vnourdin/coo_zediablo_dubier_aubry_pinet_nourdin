package behaviours;

import entity.Character;
import graphicEngine.CharacterSprite;

public class FightWithSword extends Fight{

	public FightWithSword(CharacterSprite sprite){
		super(sprite);
	}
	
	@Override
	public void attack(Character charact,int orientation,int damages){
		sprite.play(CharacterSprite.ATTACK,orientation);
		super.attack(charact, orientation, damages);
	}
}