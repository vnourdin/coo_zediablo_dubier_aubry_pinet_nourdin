package gui.pause;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import gameEngine.GameEngine;

public class OverViewPanel extends JPanel {

	private GameEngine gameEngine;
	private JPanel spritePanel, statsPanel;

	public OverViewPanel(GameEngine gameEngine) {
		this.setGameEngine(gameEngine);
		this.createPanels();
		this.manageLayout();
		this.addPanels();
	}

	private void setGameEngine(GameEngine gameEngine) {
		this.gameEngine = gameEngine;
	}

	private void createPanels() {
		this.spritePanel = new SpritePanel(this.gameEngine);
		this.statsPanel = new StatsPanel(this.gameEngine);
	}

	private void manageLayout() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
	}

	private void addPanels() {
		this.add(this.spritePanel);
		this.add(this.statsPanel);
	}
}