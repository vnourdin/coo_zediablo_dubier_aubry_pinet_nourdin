package entity;

import behaviours.FightWithExplosionSpell;
import myGame.RessourceLoader;
import environment.Labyrinth;
import graphicEngine.CharacterSprite;

public class MageWoman extends Player{
	
	public MageWoman(Labyrinth lab) {
		super(RessourceLoader.getMageWomanSprite(),lab);
		this.attackSpeed = 1000;
		this.range = 3;
		this.damages = 10;
		this.fight = new FightWithExplosionSpell((CharacterSprite) this.sprite);
	}
	
	public MageWoman(String name, Labyrinth lab) {
		super(RessourceLoader.getMageWomanSprite(),name, lab);
		this.attackSpeed = 1000;
		this.range = 3;
		this.damages = 10;
		this.fight = new FightWithExplosionSpell((CharacterSprite) this.sprite);
	}
}