package editor;

import items.Item;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import entity.Monster;
import environment.Labyrinth;

public class LevelExporter {
	
	private Labyrinth labyrinth;
	private LabyrinthExporter labyrint_exporter;
	
	public LevelExporter(Labyrinth laby){
		this.labyrinth = laby;
		this.labyrint_exporter = new LabyrinthExporterToText(laby);
	}
	
	public void export(File file) throws ParserConfigurationException, TransformerException{
		
		labyrinth.setName(file.getName());
		
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

		Document doc = docBuilder.newDocument();
		Element level = doc.createElement("level");
		
		String datasource = "Ressources/Levels/XMLLevels/"+labyrinth.getName()+".txt";
		Element labyrinth_xml = labyrinth.toXML(doc,datasource,"textfile");
		
		level.appendChild(labyrinth_xml);
		for(entity.Character c : labyrinth.getAllCharacters()){
			if(c instanceof Monster){
				level.appendChild(((Monster) c).toXML(doc));
			}
		}
		
		for(Item i : labyrinth.getAllItems()){
			level.appendChild(i.toXML(doc));
		}
		
		doc.appendChild(level);
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(file);
		
		transformer.transform(source,result);
		
		try{
			this.labyrint_exporter.exporter(datasource);
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("[+] Level saved : "+file.getAbsoluteFile());
	}
}