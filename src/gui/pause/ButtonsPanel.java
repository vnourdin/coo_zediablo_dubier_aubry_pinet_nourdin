package gui.pause;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JPanel;

import gameEngine.GameEngine;

public class ButtonsPanel extends JPanel implements ActionListener {

	private GameEngine gameEngine;
	private JButton save, load, help, quitPause, quitGame;
	private GridBagConstraints saveGBC, loadGBC, helpGBC, quitPauseGBC, quitGameGBC;

	public ButtonsPanel(GameEngine gameEngine) {
		this.setGameEngine(gameEngine);
		this.createButtons();
		this.placeButtons();
		this.addActionListenerOnButtons();
		this.addButtons();
		this.setPreferredSize(
				new Dimension(this.gameEngine.getGUI().getWidth() / 2, this.gameEngine.getGUI().getHeight()));
	}

	private void setGameEngine(GameEngine gameEngine) {
		this.gameEngine = gameEngine;
	}

	private void createButtons() {
		this.save = new JButton("Sauvegarder");
		this.save.setBackground(Color.WHITE);
		this.save.setForeground(Color.GRAY);
		this.save.setEnabled(false);
		// this.save.setIcon(RessourceLoader.saveIcon);

		this.load = new JButton("Charger une sauvegarde");
		this.load.setBackground(Color.WHITE);
		this.load.setForeground(Color.GRAY);
		this.load.setEnabled(false);
		// this.load.setIcon(RessourceLoader.loadIcon);

		this.help = new JButton("Aide");
		this.help.setBackground(Color.WHITE);
		this.help.setForeground(Color.GRAY);
		this.help.setEnabled(false);
		// this.help.setIcon(RessourceLoader.helpIcon);

		this.quitPause = new JButton("Reprendre");
		this.quitPause.setBackground(Color.WHITE);
		// this.quitPause.setIcon(RessourceLoader.quitPauseIcon);

		this.quitGame = new JButton("Quitter");
		this.quitGame.setBackground(Color.WHITE);
		// this.quitGame.setIcon(RessourceLoader.quitGameIcon);

		try {
			this.save.setFont(Font.createFont(Font.TRUETYPE_FONT, new File("Ressources/Fonts/AppleBerry.ttf"))
					.deriveFont(Font.PLAIN, 35));
			this.load.setFont(Font.createFont(Font.TRUETYPE_FONT, new File("Ressources/Fonts/AppleBerry.ttf"))
					.deriveFont(Font.PLAIN, 35));
			this.help.setFont(Font.createFont(Font.TRUETYPE_FONT, new File("Ressources/Fonts/AppleBerry.ttf"))
					.deriveFont(Font.PLAIN, 35));
			this.quitPause.setFont(Font.createFont(Font.TRUETYPE_FONT, new File("Ressources/Fonts/AppleBerry.ttf"))
					.deriveFont(Font.PLAIN, 35));
			this.quitGame.setFont(Font.createFont(Font.TRUETYPE_FONT, new File("Ressources/Fonts/AppleBerry.ttf"))
					.deriveFont(Font.PLAIN, 35));
		} catch (FontFormatException | IOException e) {
			e.printStackTrace();
		}
	}

	private void placeButtons() {
		this.setLayout(new GridBagLayout());

		this.saveGBC = new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.SOUTH, GridBagConstraints.BOTH,
				new Insets(0, 0, 5, 0), 0, 20);
		this.loadGBC = new GridBagConstraints(0, 1, 1, 1, 0, 0, GridBagConstraints.NORTH, GridBagConstraints.BOTH,
				new Insets(0, 0, 40, 0), 0, 20);
		this.helpGBC = new GridBagConstraints(0, 2, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 20);
		this.quitPauseGBC = new GridBagConstraints(0, 3, 1, 1, 0, 0, GridBagConstraints.SOUTH, GridBagConstraints.BOTH,
				new Insets(40, 0, 0, 0), 0, 20);
		this.quitGameGBC = new GridBagConstraints(0, 4, 1, 1, 0, 0, GridBagConstraints.NORTH, GridBagConstraints.BOTH,
				new Insets(5, 0, 0, 0), 0, 20);
	}

	private void addActionListenerOnButtons() {
		this.save.addActionListener(this);
		this.load.addActionListener(this);
		this.help.addActionListener(this);
		this.quitPause.addActionListener(this);
		this.quitGame.addActionListener(this);
	}

	private void addButtons() {
		this.add(this.save, this.saveGBC);
		this.add(this.load, this.loadGBC);
		this.add(this.help, this.helpGBC);
		this.add(this.quitPause, this.quitPauseGBC);
		this.add(this.quitGame, this.quitGameGBC);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.quitGame)
			System.exit(0);

		if (e.getSource() == this.quitPause)
			this.gameEngine.unpause();
	}
}