package entity;

import behaviours.FightWithSpear;
import myGame.RessourceLoader;
import environment.Labyrinth;
import graphicEngine.CharacterSprite;

public class Warrior extends Player{

	public Warrior(Labyrinth laby) {
		super(RessourceLoader.getWarriorSprite(),laby);
		this.setLife(150);
		this.damages = 15;
		this.fight = new FightWithSpear((CharacterSprite)this.sprite);
	}
	
	public Warrior(String name, Labyrinth lab)
	{
		super(RessourceLoader.getWarriorSprite(),name, lab);
		this.setLife(150);
		this.damages = 15;
		this.fight = new FightWithSpear((CharacterSprite)this.sprite);
	}
}