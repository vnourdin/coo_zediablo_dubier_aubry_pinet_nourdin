package editor;

import java.awt.Color;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ImageIcon;
import javax.swing.JToggleButton;

import entity.Entity;

public class EntityButton extends JToggleButton implements ItemListener {
	
	private Entity entity;
	private EntityController controller;
	
	public EntityButton(EntityController controller, Entity entity){
		this.entity = entity;
		this.controller = controller;
		ImageIcon icon = new ImageIcon();
		icon.setImage(this.entity.getOverview());
		this.setIcon(icon);
		this.setBackground(Color.WHITE);
		this.addItemListener(this);
	}

	@Override
	public void itemStateChanged(ItemEvent arg0) {
		if(this.isSelected()){
			this.controller.setEntity(this.entity);
		}
	}
}