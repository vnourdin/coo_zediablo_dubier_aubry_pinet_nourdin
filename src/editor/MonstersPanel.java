package editor;

import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;

import entity.ArcherSkeleton;
import entity.EnemyMage;
import entity.Monster;
import entity.OrcWarrior;
import graphicEngine.CharacterSprite;

public class MonstersPanel extends JPanel {

	private static Monster[] allMonsters =  {
		new OrcWarrior(null),
		new EnemyMage(null),
		new ArcherSkeleton(null)
	};

	public MonstersPanel(EntityController controller, LabyrinthPanel labyPanel){
		this.turnDownMonsters();
		this.setLayout(new GridLayout(2,2));
		ButtonGroup buttonGroup = new ButtonGroup();
		for(Monster monster : allMonsters){
			EntityButton monsterButton = new EntityButton(controller,monster);
			monster.addObserver(labyPanel);
			buttonGroup.add(monsterButton);
			this.add(monsterButton);
		}
	}

	private void turnDownMonsters()
	{
		for(Monster monster : allMonsters)
		{
			monster.orientate(CharacterSprite.DOWN);
		}
	}
}