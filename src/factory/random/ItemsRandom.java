package factory.random;

import items.Item;
import items.PotionToHeal;
import items.PotionToIncreaseSpeed;
import items.PotionToRegenerate;

import java.util.ArrayList;

import environment.Labyrinth;
import factory.ItemsFactory;

public class ItemsRandom implements ItemsFactory {

	
	private Item createItem(Labyrinth laby){
		int randomPotionChoice = (int)(Math.random()*3);
		Item potion;
		
		if(randomPotionChoice==0)
			potion = new PotionToIncreaseSpeed();
		else if(randomPotionChoice==1)
			potion = new PotionToHeal();
		else
			potion = new PotionToRegenerate();
		
		int posX = (int)(Math.random()*laby.getColumn());
		int posY = (int)(Math.random()*laby.getLine());
		while(laby.isSolid(posY,posX)){
			posX = (int)(Math.random()*laby.getColumn());
			posY = (int)(Math.random()*laby.getLine());
		}
			
		potion.setPosition(posX,posY);
		laby.addItem(potion);
		return potion;
	}
	
	@Override
	public ArrayList<Item> generateItems(Labyrinth laby) {
		ArrayList<Item> items = new ArrayList<Item>();
		items.add(createItem(laby));
		return items;
	}

}
