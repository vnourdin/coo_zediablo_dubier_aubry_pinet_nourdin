package ia;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import entity.Monster;

public abstract class IA{

	protected Monster sensor;
	
	public abstract Action decide();
	public abstract String xmlName();
	
	public void setSensor(Monster newSensors){
		this.sensor = newSensors;
	}
	
	public Element toXML(Document doc){
		Element ia_constant = doc.createElement("IAConstant");
		ia_constant.setTextContent(xmlName());
		return ia_constant;
	}
}