package gui;

import items.*;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;

import entity.Player;
import myGame.RessourceLoader;
import graphicEngine.Drawable;
import graphicEngine.ItemSprite;

public class GuiInventory implements Drawable{

	private Player player;
	private Inventory inventory;
	private int x,y;

	public GuiInventory(int x, int y){
		this.x = x;
		this.y = y;
	}

	@Override
	public void draw(Graphics2D g2d) {
		AffineTransform trans = g2d.getTransform();
		g2d.translate(this.x, this.y);
		for(int i=0; i<this.player.getInventoryMaxSize(); i++){
			Image imageUsed=null;
			if(i<this.inventory.size())
				imageUsed = this.determineImageToUse(this.inventory.get(i));
			else
				imageUsed = RessourceLoader.potionEmpty;
			g2d.drawImage(imageUsed, ItemSprite.ITEM_SIZE*(i%5), ItemSprite.ITEM_SIZE*(i/5), null);
		}
		g2d.setTransform(trans);
	}

	private Image determineImageToUse(Item i) {
		if (i instanceof PotionToHeal)
			return RessourceLoader.potionHeal;
		else if (i instanceof PotionToRegenerate)
			return RessourceLoader.potionRegen;
		else if (i instanceof PotionToIncreaseSpeed)
			return RessourceLoader.potionSpeed;
		else
			return RessourceLoader.potionEmpty;
	}

	public void setPlayer(Player player){
		this.player = player;
		this.inventory = this.player.getInventory();
	}

	public void setHeight(int y)
	{
		this.y = y;
	}

}