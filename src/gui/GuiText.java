package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.io.File;
import java.io.IOException;

import graphicEngine.Drawable;

public class GuiText implements Drawable {

	protected String[] contents;
	protected Font font;
	protected int x, y;

	public GuiText(int x, int y) {
		this.x = x;
		this.y = y;
		try {
			this.setFont(Font.createFont(Font.TRUETYPE_FONT, new File("Ressources/Fonts/AppleBerry.ttf")), 30,
					Font.PLAIN);
		} catch (FontFormatException | IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void draw(Graphics2D g) {
		AffineTransform svg = g.getTransform();
		g.translate(x, y);
		g.setColor(Color.BLACK);
		g.setFont(this.font);
		g.drawString(this.contents[0], 15, 10);
		g.setTransform(svg);
	}

	public void setText(String s) {
		if (s != null)
			this.contents = s.split("\n");
	}

	public void setFont(Font f, int taille, int style) {
		this.font = f.deriveFont(style, taille);
	}

	public Font getFont() {
		return this.font;
	}
}