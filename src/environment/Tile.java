package environment;

import entity.Entity;
import graphicEngine.Sprite;
import myGame.RessourceLoader;

public class Tile extends Entity {
	public static final int EMPTY=0, WALL=1, STAIRS=2, TRAP=3;
	protected int id;
	protected boolean solid;

	public Tile(int x,int y){
		super(new Sprite(RessourceLoader.ground));
		this.id = EMPTY;
		this.setPosition(x, y);
		this.solid = false;
	}

	public Tile(Sprite sprite){
		super(sprite);
		this.solid = false;
		this.setPosition(0,0);
	}

	public Tile(Sprite sprite,int posX,int posY,boolean solide){
		super(sprite);
		this.setPosition(posX, posY);
		this.solid = solide;
	}

	public int getID(){
		return this.id;
	}

	public boolean isSolid(){
		return this.solid;
	}

	public int getX(){
		return this.x;
	}

	public int getY(){
		return this.y;
	}

	@Override
	public String XMLEntityName() {
		return "tile";
	}
}