package factory.xml;

import java.util.ArrayList;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import entity.Entity;
import environment.Labyrinth;

public abstract class EntityFactoryXML<T> {

	public abstract T  createEntityFromXML(Labyrinth labyrinth,Element entityXML);
	
	public ArrayList<T> createEntityListFromXML(Labyrinth labyrinth,NodeList entity_list){
		ArrayList<T> list = new ArrayList<T>();
		for(int i=0;i<entity_list.getLength();i++){
			Node node = entity_list.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE){
				Element element = (Element)node; 
				T t = createEntityFromXML(labyrinth,element);
				setEntityPostionFromXML((Entity)t,element);
				list.add(t);
			}
		}
		return list;
	}
	
	private void setEntityPostionFromXML(Entity entity,Element entityXML){
		int posX = Integer.parseInt(entityXML.getAttribute("posX"));
		int posY = Integer.parseInt(entityXML.getAttribute("posY"));
		entity.setPosition(posX,posY);
	}
}
