package behaviours;

import entity.Character;
import graphicEngine.CharacterSprite;

public class FightWithSpear extends Fight{

	public FightWithSpear(CharacterSprite sprite) {
		super(sprite);
	}

	@Override
	public void attack(Character charact, int orientation, int damages) {
		this.sprite.play(CharacterSprite.SPEAR, orientation);
		super.attack(charact, orientation, damages);
	}
}
