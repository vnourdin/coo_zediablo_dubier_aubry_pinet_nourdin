package factory.text;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import environment.EmptyLabyrinth;
import environment.Labyrinth;
import factory.LabyrinthFactory;

public class LabyrinthTextV2 extends LabyrinthFactory{

	private String path;
	private int line;
	private int column;
	
	public LabyrinthTextV2(String path,int line,int column){
		this.path = path;
		this.line = line;
		this.column = column;
	}
	
	@Override
	public Labyrinth generateLabyrinth() throws IOException {
		this.labyrinthToBuild = new EmptyLabyrinth(line,column);
		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(path)));
		for(int i=0;i<line;i++){
			// Pour chaque case de la matrice on lit son contenu (case vide, mur ou teleporteur)
			String data_line = in.readLine();
			String [] data_colonne = data_line.trim().split(":");
			if(data_colonne.length==column){
				for(int j=0;j<column;j++){
					int id = 0;
					try{
						id = Integer.parseInt(data_colonne[j]);
					}catch(NumberFormatException e){
						System.err.println("Numéro de case invalide (case par default vide): "+id);
					}
					addCaseToLabyrinthe(createCaseFromId(j,i,id));
				}
			}
		}
		in.close();
		return this.labyrinthToBuild;
	}
}
